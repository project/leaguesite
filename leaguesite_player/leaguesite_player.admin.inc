<?php

/**
 * Gets all the current positions available in a sport and lists them.
 * @return unknown_type
 */
function leaguesite_player_positions_view(){
	$sport = arg(4);
	//Make sure the argument is a number
	if(!is_numeric($sport)){
		drupal_not_found();
	}else{
		/* get the positions available which belong to our current sport so we can list
		 * them and allow the user to delete them using checkboxes.
		 */
		$positions = db_query("SELECT * FROM {leaguesite_position} WHERE sport_id = %d ORDER BY weight;", $sport);
		$total_positions = db_fetch_array(db_query('SELECT count(pid) AS pid FROM {leaguesite_position}'));
		$total_positions = $total_positions['pid'];
		$count = 0;
		$form['positions']['#tree'] = TRUE;
		while($row = db_fetch_array($positions)){
			$form['positions'][$row['pid']]['position'] = array('#type' => 'checkbox');
			$form['positions'][$row['pid']]['weight'] = array('#type' => 'weight', '#delta' => $total_positions, '#default_value' => $row['weight'], '#attributes' => array('class' => 'leaguesite-position'));
			$form['positions'][$row['pid']]['position_name'] = array('#value' => $row['position_name'],);
			++$count;
		}
		//If we have no positions, say so. Otherwise put in a submit button.
		if($count == 0){
			$form['notice'] = array('#value' => 'No positions currently exist.');
		}else{
			$form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
			);
		}
		return $form;
	}
}

/**
 * Processes the submitted values when a user wants to remove a position
 *
 */
function leaguesite_player_positions_view_submit($form, $form_state){
	//loop through each checkbox
	foreach($form_state['values']['positions'] as $pid => $data){
		//check to see if the checkbox is ticked. If so, delete it.
		if($data['position']==1){
			db_query('DELETE FROM {leaguesite_position} WHERE pid = %d;', intval($pid));
		}
		db_query('UPDATE {leaguesite_position} SET weight = %d WHERE pid = %d', $data['weight'], $pid);
	}
}

/**
 * Theme the output of the list of leaguesite positions
 * @param unknown_type $form
 * @return unknown_type
 */
function theme_leaguesite_player_positions_view($form){
	$sport = arg(4);
	$positionrows = array();
	$output = '';

	//go through each element in the form. We are looking for the arrays which have the positions in.
	foreach ($form['positions'] as $pid => $element) {
		if(is_array($element['weight'])){
			//once we have found them, we put them in an array which we will use for the table display
			$positionrows[] = array(
        'data' => array(
			drupal_render($element['position']),
			drupal_render($element['weight']),
			drupal_render($element['position_name']),
			),
        'class' => 'draggable',
			);
		}
		//we don't want them appearing afterwards, so once we have copied them, we remove them from the original form array.
		unset($form['positions'][$pid]);
	}
	//this is the header for the table.
	$positionheader = array(t('Tick to Remove'), t('Weight'), t('Position'));
	//as long was we have some positions, we can display them in a table. If we don't we won't display the table.
	if(count($positionrows) > 0){
		$output = theme('table', $positionheader, $positionrows, array('id' => 'leaguesite-position-table'));
		drupal_add_tabledrag('leaguesite-position-table', 'order', 'sibling', 'leaguesite-position');
	}
	$output .= drupal_render($form);
	$output .= '<br />'.l('Add a position to this sport', 'admin/content/leaguesite/sport/'.$sport.'/add/position');
	return $output;
}

/**
 * Returns a form to add a position to a sport.
 * @return $form - the form which the user should fill out.
 */
function leaguesite_player_position_add(){
	$sport = arg(4);
	$breadcrumb = drupal_get_breadcrumb();
	$breadcrumb[] = l(t('Edit Sport'), 'admin/content/leaguesite/sport/'.$sport.'/edit/positions');
	drupal_set_breadcrumb($breadcrumb);
	$form = array();
	if(is_numeric($sport)){
		$form = array();
		$form['position_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t('The name of the position, e.g. centre forward'),
      '#size' => 30,
      '#maxlength' => 50,
      '#required' => TRUE,
		);
		$form['submit'] = array(
      '#type' => 'submit', '#value' => t('Add Position')
		);
	}
	return $form;
}

/**
 * Takes submitted form and creates a position for the sport.

 */
function leaguesite_player_position_add_submit($form, $form_state){
	$sport = arg(4);
	$position_name = $form_state['values']['position_name'];
	if(is_numeric($sport)){
		db_query("INSERT INTO {leaguesite_position} (position_name, sport_id) VALUES ('%s', %d)", $position_name, $sport);
		drupal_set_message('Position added successfully.');
		drupal_goto('admin/content/leaguesite/sport/'.$sport.'/edit/positions');
	}
}

/**
 * Get players in a team so we can view them.
 * @return unknown_type
 */
function leaguesite_players_view_in_team(){
	$team = arg(1);
	if(!is_numeric($team)){
		drupal_not_found();
	}else{
		/*
		 * Its important to bear in mind that teams can be added to multiple sports,
		 * so players might not be playing the same games!
		 * This database query will get all the players. Technically, we should look at the sports
		 * that the team is playing in and if there is more than one, allow the user to
		 * select the sport the player plays for when adding them, but I assume the general use of this
		 * module will be one sport, so this is not a feature I will implement unless specifically requested; It will just make the interface more confusing.
		 * Each season the team plays in should be in a collapsible pane which means different
		 * players can be assigned to a team for different seasons.
		 */
		$players = db_query("SELECT {leaguesite_player_team}.*, {leaguesite_player_team}.position, {leaguesite_season}.name AS season_name, {leaguesite_season}.sid, {leaguesite_season}.end, {leaguesite_player}.last_name, {leaguesite_player}.first_name, {node}.title FROM {leaguesite_player_team} LEFT JOIN {leaguesite_season} ON {leaguesite_player_team}.season_id = {leaguesite_season}.sid LEFT JOIN {leaguesite_player} ON {leaguesite_player_team}.player_id = {leaguesite_player}.player_id lEFT JOIN {node} ON {leaguesite_player}.nid = {node}.nid WHERE team_id = %d ORDER BY {leaguesite_season}.end, {leaguesite_player}.last_name, {leaguesite_player}.first_name;", $team);
		$count = 0;
		$positions = array();
		$positions[-1] = t('None');
		$position_db = db_query('SELECT {leaguesite_position}.position_name, {leaguesite_position}.pid FROM {leaguesite_position} LEFT JOIN {leaguesite_sports} ON {leaguesite_position}.sport_id = {leaguesite_sports}.sport_id LEFT JOIN {leaguesite_league} ON {leaguesite_sports}.sport_id = {leaguesite_league}.sport_id LEFT JOIN {leaguesite_relation} ON {leaguesite_league}.lid = {leaguesite_relation}.lid LEFT JOIN {leaguesite_standings} ON {leaguesite_relation}.relation_id = {leaguesite_standings}.relation_id WHERE team_id = %d GROUP BY {leaguesite_position}.position_name;', $team);
		while($row = db_fetch_array($position_db)){
			$positions[$row['pid']] = $row['position_name'];
		}

		while($row = db_fetch_array($players)){
			$form[$row['sid'].'fieldset'] = array(
    	 '#type' => 'fieldset',
    	 '#collapsible' => TRUE,
    	 '#title' => $row['season_name'],
			);
			if($row['end'] < time()){
				$form[$row['sid'].'fieldset']['#collapsed'] = TRUE;
			}
			 
			$form[$count]['player_id'.$count] = array('#type' => 'hidden', '#value' => $row['player_id']);
			$form[$count]['last_name'.$count] = array('#value' => $row['last_name']);
			$form[$count]['season_id'.$count] = array('#type' => 'hidden', '#value' => $row['sid']);
			$form[$count]['first_name'.$count] = array('#value' => $row['first_name']);
			$form[$count]['title'.$count] = array('#value' => $row['title']);
			//Only add positions if there are ones to add. Otherwise we will miss out the column.
			if(count($positions) > 1){
				if($row['position'] != -1){
					$form[$count]['position'.$count] = array('#type' => 'select', '#options' => $positions, '#default_value' => $row['position']);
				}else{
					$form[$count]['position'.$count] = array('#type' => 'select', '#options' => $positions);
				}
			}
			++$count;
		}
		if($count == 0){
			$form['notice'] = array('#value' => 'There are no players in this team for any season.');
		}else{
			if(count($positions) > 1){
				$form['submit'] = array('#type' => 'submit', '#value' => t('Save Changes'));
			}
		}
		return $form;
	}
}//end function leaguesite_players_view_in_team()

/**
 * Theme the output of the list of leaguesite players and positions
 * @param unknown_type $form
 * @return unknown_type
 */
function theme_leaguesite_players_view_in_team($form){
	$team = arg(1);
	$positionrows = array();
	$count = 0;
	$season = array();
	//Go through all the form elements looking for players. Add them to a table setup
	foreach ($form as $name => $element) {
		 
		 
		if(is_array($element['player_id'.$name])){
			if(isset($element['position'.$name])){
				$positionrows[$element['season_id'.$name]['#value']][] = array(
				drupal_render($element['title'.$name]),
				drupal_render($element['position'.$name]),
				);
			}else{
				$positionrows[$element['season_id'.$name]['#value']][] = array(
				drupal_render($element['title'.$name]),
				);
			}
			//Once we have added the players, we don't want to display them again, so we remove them from the original form.
			unset($form[$name]['player_id'.$name]);
			unset($form[$name]['last_name'.$name]);
			unset($form[$name]['first_name'.$name]);
			unset($form[$name]['position'.$name]);
			unset($form[$name]['title'.$name]);
			++$count;
		}
	}
	//loop through each season_id and add it to the fieldset which would have been created in the form construction.
	foreach ($positionrows as $season_id => $rows){
		//Check how many columns we have. Only had the position column if we are rendering it.
		if(count($rows[0]) == 1){
			$positionheader = array(t('Name'));
		}else{
			$positionheader = array(t('Name'), t('Usual Position'));
		}
		if(count($rows) > 0){
			$output = theme('table', $positionheader, $rows);
		}
		$form[$season_id.'fieldset']['list'] = array('#type' => 'markup', '#value' => $output);
	}

	$output = drupal_render($form);
	$output .= '<br />'.l(t('Click here to add and remove players.'), 'node/'.$team.'/add/players');
	return $output;
}// end function theme_leaguesite_players_view_in_team()

/**
 * leaguesite_players_view_in_team_submit()
 * Takes the positions of the players which are chosen in a dropdown box and saves the changes.
 */
function leaguesite_players_view_in_team_submit($form, $form_state){
	$team = intval(arg(1));
	$count = 0;
	while(isset($form_state['values']['player_id'.$count])){
		//loop through while there are still players and save the positions that have been selected.
		if($form_state['values']['position'.$count] == -1){
			$form_state['values']['position'.$count] = -1;
		}
		db_query('UPDATE {leaguesite_player_team} SET position = %d WHERE player_id = %d AND season_id = %d AND team_id = %d;', intval($form_state['values']['position'.$count]), intval($form_state['values']['player_id'.$count]), intval($form_state['values']['season_id'.$count]), $team);
		++$count;
	}
}//end function leaguesite_players_view_in_team_submit

/**
 * Function leaguesite_players_add_to_team - displays a grid of checkboxes, like the permissions page, which allows a user to add players to teams for each season.
 * @return unknown_type
 */
function leaguesite_players_add_to_team(){
	$team = node_load(intval(arg(1)));
	$breadcrumb = drupal_get_breadcrumb();
	$breadcrumb[] = l($team->title, 'node/'.$team->nid.'/edit');
	drupal_set_breadcrumb($breadcrumb);
	drupal_set_title(t('Add Players to ').$team->title);
	//Firstly, get all the players and seasons, then go through and check the ones who are in teams.
	$players = db_query("SELECT {node}.*, {leaguesite_player}.player_id FROM {node} LEFT JOIN {leaguesite_player} ON {node}.nid = {leaguesite_player}.nid WHERE {node}.type = 'leaguesite_player' ORDER BY {leaguesite_player}.last_name ASC, {leaguesite_player}.first_name ASC, {node}.title");

	//We want to get all the seasons and allow a team to have players entered regardless of whether they are part of that season in any league. This allows players to be added for friendly games in a season
	$seasons_list = db_query('SELECT name, {leaguesite_season}.sid FROM {leaguesite_season} ORDER BY end DESC;');
	$seasons = array();

	while($season = db_fetch_array($seasons_list)){
		$seasons[$season['sid']] = $season['name'];
		$form['seasons'][$season['sid']] = array(
  	  '#type' => 'label',
  	  '#value' => $season['name'],
		);
	}
	//TODO: Tidy ths up for proper display!
	if(count($seasons) == 0){
		//we can't add players yet! Get the user to add a season first.
		$form = array();
		$form['notice'] = array('title' => t('You haven\'t added any seasons yet! Please add some before you add players, as a player must be added to a team in a season'));
	}else{
		//Put the seasons into an array we can loop into when adding checkboxes
		$count = 0;
		//loop through and add the players as checkboxes for people to tick and then add a submit button at the bottom. Simples.
		while($row = db_fetch_array($players)){
			$form['players'][$row['player_id']]['label'] = array(
      '#title' => $row['title'],
			);
			foreach($seasons as $id => $name){
				//add the checkbox and give it a unique name - the season id, and the player id separated by a decimal point.
				$form['players'][$row['player_id']]['seasons'][$id.'n'.$row['player_id']] = array(
        '#type' => 'checkbox',
				);
			}
			++$count;
		}

		//Go through all the checkboxes and check the players who are in this team.
		$players = db_query('SELECT player_id, season_id FROM {leaguesite_player_team} WHERE team_id = %d', $team->nid);
		while($row = db_fetch_array($players)){
			//check all the relevant checboxes.
			$form['players'][$row['player_id']]['seasons'][$row['season_id'].'n'.$row['player_id']]['#default_value'] = 1;
		}
		if($count == 0){
			$form['players']['label'] = array(
      '#value' => t('Either all players are added to your team, or none exist! Go to ').l(t('create player'), 'node/add/leaguesite-player').t(' to create some'),
			);
		}else{
			$form['submit'] = array(
      '#type' => 'submit', '#value' => t('Add Players to Team')
			);
		}
	}
	return $form;
}//end function leaguesite_players_add_to_team()

/*
 * Thie function themes a grid of checkboxes, so that a player can be added to a team per season.
 * @see leaguesite_players_add_to_team()
 */
function theme_leaguesite_players_add_to_team($form){
	//get seasons for the checkboxes to be listed by
	$seasons = array();
	$seasons[] = '';
	$rows = array();
	foreach($form['seasons'] as $id => $data){
		if(is_numeric($id) && isset($data['#value'])){
			$seasons[] = $data['#value'];
		}
	}
	foreach($form['players'] as $id => $data){
		if(is_numeric($id) && isset($data['label'])){

			$row = array();
			$row[] = check_plain($data['label']['#title']);
			foreach($data['seasons'] as $season_id => $checkbox){

				$ids = explode('n', $season_id, 2);
				if(is_numeric($ids[0]) && is_numeric($ids[1])){
					$row[] = drupal_render($checkbox);
				}
			}
			$rows[] = $row;
			unset($row);
			unset($form['players'][$id]);
		}
	}

	$output = theme('table', $seasons, $rows);
	$output .= drupal_render($form);
	return $output;
}

/**
 * Function to add the players to a team. See leaguesite_players_add_to_team for the form creation.
 * @return unknown_type
 */
function leaguesite_players_add_to_team_submit($form, $form_state){
	/*
	 * The checkboxes are sent through and contain two numbers separated by a 'n' to uniqueley identify them. The first number is the season Id, and the second is the player id. I call explode on them to get the two values.
	 */
	$team = arg(1);
	if(!is_numeric($team)){
		drupal_not_found();
	}else{
		$addcount = 0;
		$removecount = 0;
		//loop through the form league values and add the scoring method if checked
		foreach($form_state['values'] as $id => $checked){
			$info = explode('n', $id);
			 
			//we don't have to worry about unchecked boxes, just checked ones.
			if($checked == 1){
				//if checked equals 1, then add the relation
				//I enter a player into a team here, but I should avoid duplicated, therefore if an update returns a value, then I should not insert as a record exists.
				if(!db_fetch_array(db_query('SELECT * FROM {leaguesite_player_team} WHERE player_id = %d AND team_id = %d AND season_id = %d', $info[1], $team, $info[0]))){
					db_query("INSERT INTO {leaguesite_player_team} (player_id, team_id, season_id, position) VALUES (%d, %d, %d, %d)", intval($info[1]), $team, intval($info[0]), '-1');
					++$addcount;
				}
			}else{
				if(db_fetch_array(db_query('SELECT * FROM {leaguesite_player_team} WHERE player_id = %d AND team_id = %d AND season_id = %d', $info[1], $team, $info[0]))){
					db_query('DELETE FROM {leaguesite_player_team} WHERE team_id = %d AND player_id = %d AND season_id = %d', $team, intval($info[1]), intval($info[0]));
					++$removecount;
				}
			}
		}
		drupal_set_message($addcount.' '.t("players added to team"));
		drupal_set_message($removecount.' '.t("players removed from team"));
	}
}


/**
 * Function to get the teams of players in a game and list them, so the user can select which positions the players are playing in.
 * The user can also administer when a player has come into the game, and when the player has left the game.
 * Also, a user will be able to add details such as when a player recieved a yellow card, or scored a goal.
 */
function leaguesite_players_view_in_game(){
	$node = node_load(intval(arg(1)));
	//Get a list of players from the database in the same season as the current game.
	//Display the names on the left, and the positions on the right. Users should be able to select a position, or none.
	//Players who are selected with 'none' will not appear on the match breakdown page.
	//Each team will have a collapsible fieldset with the players for them inside.


	$teams = db_query('SELECT {node}.title, {leaguesite_player}.player_id, {node}.nid FROM {node} LEFT JOIN {leaguesite_player} ON {node}.nid = {leaguesite_player}.nid WHERE {node}.nid = %d OR {node}.nid = %d;', $node->home_team, $node->away_team);
	while($team = db_fetch_array($teams)){
		$form['teams'][$team['nid']] = array('#type' => 'fieldset', '#title' => $team['title'], '#collapsible' => TRUE);
	}

	//get a list of players for the current season, only for the two teams that are playing.
	$players = db_query('SELECT {node}.*, {leaguesite_player}.*, {leaguesite_player_team}.position, {leaguesite_player_team}.team_id FROM {node} LEFT JOIN {leaguesite_player} ON {node}.nid = {leaguesite_player}.nid LEFT JOIN {leaguesite_player_team} ON {leaguesite_player}.player_id = {leaguesite_player_team}.player_id LEFT JOIN {leaguesite_relation} ON {leaguesite_player_team}.season_id = {leaguesite_relation}.sid WHERE {leaguesite_relation}.relation_id = %d AND ({leaguesite_player_team}.team_id = %d OR {leaguesite_player_team}.team_id = %d) ORDER BY {leaguesite_player}.last_name;', $node->season_league, $node->home_team, $node->away_team);

	//For when this form has been submitted before, we construct an array of the positions as they were saved. We then access these when constructing the form
	$position_details = db_query('SELECT * FROM {leaguesite_player_match} WHERE match_id = %d;', $node->nid);
	$count = 0;
	while($row = db_fetch_array($position_details)){
		$previous_details[$row['team_id']][$row['player_id']] = $row['position'];
		++$count;
	}
	//check if there are details, and therefore if we are inserting new values or updating values.
	if($count > 0){
		$state = 'update';
	}else{
		$state = 'insert';
	}

	$positions = array();
	$positions['none'] = t('Not playing');
	$positions_query = db_query('SELECT * FROM {leaguesite_position} WHERE sport_id = %d', intval($node->sport));
	while($position = db_fetch_array($positions_query)){
		$positions[$position['pid']] = $position['position_name'];
	}
	//If there are no positions then allow the user to select 'playing' or 'not playing' as their options.
	if(count($positions) == 1){
		$positions['-1'] = t('Playing');
	}
	$count = 0;
	while($row = db_fetch_array($players)){
		//get the players and sort them into an array, separated by their team ID. Place them in a table with their default positions, allowing the user to select which players are playing.
		$form['players'][$count]['player_id'.$count] = array('#type' => 'hidden', '#value' => $row['player_id']);
		$form['players'][$count]['team_id'.$count] = array('#type' => 'hidden', '#value' => $row['team_id']);
		$form['players'][$count]['name'.$count] = array('#value' => $row['title']);
		if(isset($previous_details[$row['team_id']][$row['player_id']])){
			$form['players'][$count]['position'.$count] = array('#type' => 'select', '#options' => $positions, '#default_value' => $previous_details[$row['team_id']][$row['player_id']]);
		}else if($state == 'insert'){
			$form['players'][$count]['position'.$count] = array('#type' => 'select', '#options' => $positions, '#default_value' => $row['position']);
		}else if($state == 'update'){
			//If we are updating, and the player wasn't playing before, we set his position as 'Not playing' regardless of their team position
			$form['players'][$count]['position'.$count] = array('#type' => 'select', '#options' => $positions, '#default_value' => 'none');
		}
		++$count;
		//add the dropdown boxes for the players positions which will determine whether they will play a part in this game.

	}

	$form['submit'] = array(
	  '#type' => 'submit',
	  '#value' => t('Save'),
	);

	return $form;
}

/**
 * This themes the players in a game and adds the players into a fieldset then lists them in a table inside the fieldset.
 * @param $form - The form to be rendered
 * @return unknown_type
 */
function theme_leaguesite_players_view_in_game($form){
	//The fieldsets for the teams are contained in $form['teams']
	$teams = array();
	foreach($form['teams'] as $id => $data){
		if(is_numeric($id) && is_array($data)){
			$teams[] = $id;
		}
	}

	$rows = array();
	$count = 0;

	while(isset($form['players'][$count]['player_id'.$count])){
		$row = array();
		$row[] = drupal_render($form['players'][$count]['name'.$count]);
		$row[] = drupal_render($form['players'][$count]['position'.$count]);
		 
		 
		$rows[$form['players'][$count]['team_id'.$count]['#value']][] = $row;
		//remove the variables now they have been copied to the table
		unset($form['players'][$count]);
		++$count;
	}
	$header = array('', '');

	if(count($rows[$teams[0]]) != 0){
		$output = theme('table', $header, $rows[$teams[0]]);
		$form['teams'][$teams[0]]['list'] = array('#type' => 'markup', '#value' => $output);
	}else{
		$form['teams'][$teams[0]]['list'] = array('#type' => 'markup', '#value' => l('No players in team! To add some, click here', 'node/'.$teams[0].'/add/players'));
	}

	if(count($rows[$teams[1]]) != 0){
		$output = theme('table', $header, $rows[$teams[1]]);
		$form['teams'][$teams[1]]['list'] = array('#type' => 'markup', '#value' => $output);
	}else{
		$form['teams'][$teams[1]]['list'] = array('#type' => 'markup', '#value' => l('No players in team! To add some, click here', 'node/'.$teams[1].'/add/players'));
	}


	$output = drupal_render($form);
	return $output;

}

function leaguesite_players_view_in_game_submit($firm, $form_state){
	$count = 0;
	$match = intval(arg(1));
	//I need to establish whether we are updating the table or inserting values. A simple db_query tells us this. If we recieve rows, then we should update.
	if(db_fetch_array(db_query('SELECT id FROM {leaguesite_player_match} WHERE match_id = %d;', $match))){
		$state = 'update';
	}else{
		$state = 'insert';
	}
	while(isset($form_state['values']['player_id'.$count])){
		if($form_state['values']['position'.$count] != 'none'){
			//The player is taking a part in the match. They can be added to the database for the game.
			//A position of -1 means the sport does not have positions associated with it.
			//check if we are updating or inserting
			if($state == 'update'){
				db_query('UPDATE {leaguesite_player_match} SET position = %d WHERE match_id = %d AND team_id = %d AND player_id = %d;', intval($form_state['values']['position'.$count]), $match, intval($form_state['values']['team_id'.$count]), intval($form_state['values']['player_id'.$count]));
				$updated_rows = db_affected_rows();
			}
			if($state == 'insert' || $updated_rows == 0){
				//We insert if the user is saving this for the first time (insert) OR if the player wasn't in the team in the previous save, which is why we check the affected rows
				db_query('INSERT INTO {leaguesite_player_match} (player_id, team_id, match_id, position) VALUES (%d, %d, %d, %d);', intval($form_state['values']['player_id'.$count]), intval($form_state['values']['team_id'.$count]), $match, intval($form_state['values']['position'.$count]));
			}
		}else{
			//If we are updating, we should ensure that this player who is not playing is not in the table from a previous save.
			if($state == 'update'){
				db_query('DELETE FROM {leaguesite_player_match} WHERE player_id = %d AND team_id = %d AND match_id = %d;', intval($form_state['values']['player_id'.$count]), intval($form_state['values']['team_id'.$count]), $match);
			}
		}
		++$count;
	}

}
