<?php
function leaguesite_player_views_data(){
  
	$data['leaguesite_player']['table']['group'] = t('LeagueSite Player');
  $data['leaguesite_player']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'table' => 'leaguesite_player',
      'field' => 'nid',
    ),
  );
  $data['leaguesite_player']['first_name'] = array(
    'title' => t('First name'),
    'help' => t('The first name of the player'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_in_operator',
    ),
  );
  $data['leaguesite_player']['last_name'] = array(
    'title' => t('Last name'),
    'help' => t('The last name of the player'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_in_operator',
    ),
  );
  $data['leaguesite_player']['weight'] = array(
    'title' => t('Player weight'),
    'help' => t('Sort by the user assigned weight of a player'),
    'sort' => array(
      'handler' => 'views_handler_sort'
    ),
  );
  $data['leaguesite_player']['active'] = array(
    'title' => t('Is active'),
    'help' => t('Represents if the player is active or not'),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator'
    ),
  );
  
  //leaguesite_position
  //There are two relations here, one for the game position and one for the team position.
  if(module_exists('leaguesite_match')){
    $data['leaguesite_position_match']['table']['group'] = t('LeagueSite Player');
    $data['leaguesite_position_match']['table']['join'] = array(
      'node' => array(
        'table' => 'leaguesite_position',
        'left_table' => 'leaguesite_player_match',
        'left_field' => 'position',
        'field' => 'pid',
      ),
    );
    $data['leaguesite_position_match']['position_name'] = array(
      'title' => t('Match position'),
      'help' => t('The position of the player in a match'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
    );
    $data['leaguesite_position_match']['pid'] = array(
      'title' => t('Match position id'),
      'help' => t('The position id of the player in a match'),
      'sort' => array(
        'handler' => 'views_handler_sort_numeric',
      ),
    );
    $data['leaguesite_position_match']['weight'] = array(
      'title' => t('Match position weight'),
      'help' => t('The weight of a position'),
      'sort' => array(
        'handler' => 'views_handler_sort_numeric',
      ),
    );
      //leaguesite_player_match
      $data['leaguesite_player_match']['table']['group'] = t('Leaguesite Player');
      $data['leaguesite_player_match']['table']['join'] = array(
        'node' => array(
          'left_table' => 'leaguesite_player',
          'left_field' => 'player_id',
          'field' => 'player_id',
        ),
      );
      $data['leaguesite_player_match']['team_id'] = array(
      'title' => t('Team name in match'),
        'help' => t('Gives you the team ID of the player when listing players in a match.'),
        'relationship' => array(
          'base' => 'node',
          'base field' => 'nid',
          'handler' => 'views_handler_relationship',
          'label' => t('Team name in match'),
        ),
      );

      $data['leaguesite_player_match']['match_id'] = array(
      'title' => t('Match ID'),
        'help' => t('Use as an argument for filtering when viewing a match.'),
        'argument' => array(
          'handler' => 'views_handler_argument_numeric',
          'label' => t('Id of the match'),
        ),
      );
      
      /* fields need continuing.
       * match should be available as an argument and a relation?
       * Team should be a relation
       * Think of the views we would want to create.
       * Players and their positions in a match
       * ...
       */
  }
  
  $data['leaguesite_position_team']['table']['group'] = t('LeagueSite Player');
  $data['leaguesite_position_team']['table']['join'] = array(
    'node' => array(
      'table' => 'leaguesite_position',
      'left_table' => 'leaguesite_player_team',
      'left_field' => 'position',
      'field' => 'pid',
    ),
  );
  $data['leaguesite_position_team']['position_name'] = array(
    'title' => t('Team position'),
    'help' => t('The position of a player in a team'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  $data['leaguesite_position_team']['pid'] = array(
    'title' => t('Match position id'),
    'help' => t('The position id of the player in a team'),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
  );
  $data['leaguesite_position_team']['weight'] = array(
    'title' => t('Team position weight'),
    'help' => t('The weight of a position'),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
  );
  
  
  //leaguesite_player_team
  $data['leaguesite_player_team']['table']['group'] = t('Leaguesite Player');
  $data['leaguesite_player_team']['table']['join'] = array(
    'node' => array(
      'left_table' => 'leaguesite_player',
      'left_field' => 'player_id',
      'field' => 'player_id',
    ),
  );
  $data['leaguesite_player_team']['team_id'] = array(
    'title' => t('Player Team ID'),
    'help' => t('Use as an argument to filter players when viewing a team.'),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'label' => t('Player team ID'),
    ),
  );
  
  /*
  //leaguesite_player_1 and leaguesite_player_2 are used for match breakdown display.
  $data['leaguesite_player_1']['table']['group'] = t('LeagueSite Match Breakdown');
  $data['leaguesite_player_1']['table']['join'] = array(
    'node' => array(
      'table' => 'leaguesite_player',
      'field' => 'nid',
    ),
  );
  */
  return $data;
}
