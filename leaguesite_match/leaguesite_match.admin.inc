<?php

function leaguesite_match_admin(){
	$output = '';
	$output .= drupal_get_form('leaguesite_match_filter');
	$output .= drupal_get_form('leaguesite_match_list');
	return $output;
}

/*
 * Entering multiple matches:
 * I should be able to create an object $node and populate it with the essential leaguesite items.
 * Calling node_submit($node) then prepares the node for save.
 * Calling node_save($node) will allow the node to be created if it is new.
 * All fields I add will be added to the leaguesite_match database through hook_save()
 *
 */

function leaguesite_match_create_admin($form){
	$form_selection = $form['form_selection'];
	$form = array();

	$matchday = array();
	$team_list = array();

	$form['#cache'] = TRUE;
	//This form will use AHAH callbacks for when a season / league combo is selected. There will be a list at the top,
	//allowing a user to select the season/league and then a listbox allowing the user to select the amount of matches they are entering, up to a max of 20?
	$form['header'] = array(
	  '#type' => 'fieldset',
	  '#title' => t('Please select the season and number of matches you wish to create.'),
	//I am re-using a theme from the node match creation page here, but it shouldn't matter as all I need it to do is add a CSS file.
	  '#theme' => 'leaguesite_match_options'
	  );
	  $season_list = _leaguesite_match_get_season_list();
	  $default_season_value = $form_selection['season_league'];
	  $default_number_value = $form_selection['matches'];
	  $form['matches'] = array(
    '#type' => 'fieldset',
    '#title' => t('Matches'),
    '#prefix' => '<div id="leaguesite-match_create-wrapper">',
    '#suffix' => '</div>',
    '#theme' => 'leaguesite_matches_create'
    );
    $form['matches']['label']= array(
    '#type' => 'label',
    '#weight' => -1,
    '#value' => t('Please select the league and the number of games you want to create, and they will appear below.'),
    );
    if(isset($form_selection)){
    	//This means the form is being resubmitted because of AJAX. I will read through the form and construct as appropriate.

    	/*
    	 * Check if the season/league has been selected and the number of matches selected also.
    	 * If so, this means we need to ensure that we get the teams
    	 * We then set up a value to loop through, to lay out the form to create the matches on.
    	 * We will not need the season/league selection, but we should have date, time, matchday (if applicable) home_team and away_team.
    	 */
    	if($default_number_value > 0){
    		//find out if the season relation is a friendly or not, get the teams related to it if not.
      $type = db_fetch_array(db_query('SELECT relation_type FROM {leaguesite_relation} WHERE relation_id = %d;', intval($default_value)));
      if($type['relation_type'] == 'friendly'){
      	//get all the teams for the team list
      	$teams = db_query("SELECT title, nid FROM {node} WHERE type ='leaguesite_team' ORDER BY title;");
      	$team_list = array();
      	while($team = db_fetch_array($teams)){
      		$team_list[$team['nid']] = $team['title'];
      	}
      	//set the variables to hide the matchday option
      	$matchday = array(
          '#type' => 'hidden',
          '#default_value' => -1,
      	);
      }else{
      	//get the number of weeks in the selected league.
      	//if there are no weeks, leave the matchday selection out.
      	$matchdays = array();
      	$weeks = db_fetch_array(db_query('SELECT weeks FROM {leaguesite_league} LEFT JOIN {leaguesite_relation} ON {leaguesite_league}.lid = {leaguesite_relation}.lid WHERE {leaguesite_relation}.relation_id = %d', $default_season_value));
      	if($weeks['weeks'] > 0){
      		for($count = 1; $count <= $weeks['weeks']; ++$count){
      			$matchdays[$count] = $count;
      		}
      		$matchday = array(
            '#type' => 'select',
            '#required' => FALSE,
            '#options' => $matchdays,
            '#weight' => 0,
            '#default_value' => $form_selection['matchday'.$count],
      		);
      	}else{
      		$matchday = array(
            '#type' => 'hidden',
            '#default_value' => -1,
      		);
      	}
      	//just get the teams in the league for the team list
      	$teams = db_query("SELECT title, nid FROM {node} LEFT JOIN {leaguesite_standings} ON {node}.nid = {leaguesite_standings}.team_id WHERE {node}.type = 'leaguesite_team' AND {leaguesite_standings}.relation_id = %d ORDER BY title;", $default_season_value);
      	$team_list = array();
      	while($team = db_fetch_array($teams)){
      		$team_list[$team['nid']] = $team['title'];
      	}
      	if(count($team_list) == 0){
      		$team_list[] = 'No teams!';
      	}
      }
    	}

    	for($count = 0; $count < $default_number_value; ++$count){
      $form['matches']['time'.$count] = array(
        '#type' => 'date_popup',
        '#date_format' => variable_get('leaguesite_match_edit_date_format', 'd.m.Y.H:i'),
        '#date_year_range' => '-3:+5',
      //TODO: Enter the default time into the date. I used the lines below to do it on the hook_form in leaguesite_match.
      //$current_date = date_format_date(date_make_date('now', 'NULL', 'DATE_UNIX'), 'custom', 'Y-m-d');
      //$current_date .= ' '.variable_get('leaguesite_match_start_time_hour', '14').':'.variable_get('leaguesite_match_start_time_minute', '00');
        '#weight' => -6,
        '#default_value' => $form_selection['time'.$count],
      );

      $form['matches']['matchday'.$count] = $matchday;
      $form['matches']['home'.$count] = array(
        '#type' => 'select',
        '#required' => FALSE,
        '#options' => $team_list,
        '#weight' => 0,
        '#default_value' => $form_selection['home'.$count],
      );
      $form['matches']['away'.$count] = array(
        '#type' => 'select',
        '#required' => FALSE,
        '#options' => $team_list,
        '#weight' => 0,
        '#default_value' => $form_selection['away'.$count],
      );
    	}
    }
    $form['header']['season_league'] = array(
    '#type' => 'select',
    '#title' => t('League and season'),
    '#required' => TRUE,
    '#options' => $season_list,
    '#weight' => -2,
    '#default_value' => $default_season_value,
    '#ahah' => array(
      'path' => 'leaguesite/match_create_options',
      'wrapper' => 'leaguesite-match_create-wrapper',
      'event' => 'change',
    ),
    );
    $number = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20');
    $form['header']['matches'] = array(
    '#type' => 'select',
    '#title' => t('No. of matches'),
    '#required' => TRUE,
    '#options' => $number,
    '#weight' => -1,
    '#default_value' => $default_number_value,
    '#ahah' => array(
      'path' => 'leaguesite/match_create_options',
      'wrapper' => 'leaguesite-match_create-wrapper',
      'event' => 'change',
    ),
    );
    //TODO: left this here on Friday night!!
    $form['header']['create_form'] = array(
    '#type' => 'submit',
    '#hidden' => TRUE,
    '#value' => 'submit_category',
    '#prefix' => '<div id="leaguesite_hidden_submit">',
    '#suffix' => '</div>',
    '#submit' => array('leaguesite_create_matches_submit'),
    '#weight' => 4,
    );

    $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    );

    return $form;
}

function leaguesite_match_create_admin_validate($form, $form_state){
	//validate all the match options here.
}


function leaguesite_match_create_admin_submit($form, $form_state){
	global $user;
	//Check the clicked button, we don't want to process submits for AHAH callbacks.
	if($form_state['clicked_button']['#id'] == 'edit-submit' && node_access('create', 'leaguesite_match')){
		$count = 0;

		while(isset($form_state['values']['time'.$count]) && $form_state['values']['time'.$count]['date'] != ''){
				
			//create and enter basic details for the node.
			$node->type = 'leaguesite_match';
			$node->match_time = $form_state['values']['time'.$count];
			$node->home_team = $form_state['values']['home'.$count];
			$node->away_team = $form_state['values']['away'.$count];
			$node->matchday = $form_state['values']['matchday'.$count];
			$node->season_league = $form_state['values']['season_league'];
			$node->home_score = '';
			$node->away_score = '';
			$node->name = $user->name;
			$node->uid = $user->uid;
			//prepare the node to be saved - this creates any fields I have not created that are needed.
			$node = node_submit($node);
			//save the node - this will call hook_insert throughout Drupal and deal with the rest.
			node_save($node);
			++$count;
			unset($node);
		}
		drupal_set_message($count.' matches created.');
	}
}

/**
 * This function is to display a form for entering the result to a match.
 * It used to be on the main node edit page, but the development of more
 * complex results has meant it is going to be added as a separate page.
 */
function leaguesite_match_score_edit(){
	$node = node_load(arg(1));
	$form = NULL;

	/**
	 * invoke a method for another module here? One for the match results
	 * If there is no result, then the module isn't enabled, or there are no match attributes
	 * Just show the basic form in this case
	 */
	if(module_exists('leaguesite_scoring')){
		$form = leaguesite_scoring_load_form($node);
	}
	if($form == NULL){
		$form['results']['home_score'] = array(
      '#type' => 'textfield',
      '#size' => 3,
      '#required' => FALSE,
      '#maxlength' => 3,
      '#default_value' => $node->home_score,
		);

		$form['results']['away_score'] = array(
      '#type' => 'textfield',
      '#required' => FALSE,
      '#maxlength' => 3,
      '#size' => 3,
      '#default_value' => $node->away_score,
		);
	}
	$form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
	);
	if(module_exists('leaguesite_scoring')){
		$form['submit_to_breakdown'] = array(
      '#type' => 'submit',
      '#value' => t('Save and enter breakdown'),
		);
	}
	return $form;
}

function theme_leaguesite_match_score_edit($form){
	$attributes = array();

	//check if there are any attributes, or if we just need the whole score
	if(module_exists('leaguesite_scoring') && isset($form['results']['attid0'])){
		$count = 0;
		while (isset($form['results']['attid'.$count])){
			$attributes[] = array(
			check_plain($form['results']['attname'.$count]['#value']),
			drupal_render($form['results']['homeattval'.$count]),
			drupal_render($form['results']['awayattval'.$count]),
			);
			unset($form['results']['attname'.$count]);
			unset($form['results']['homeattval'.$count]);
			unset($form['results']['awayattval'.$count]);
			++$count;
		}
	}else{
		//Just the one line for the score then.
		$attributes[] = array(
		t('Score'),
		drupal_render($form['results']['home_score']),
		drupal_render($form['results']['away_score']),
		);
		unset($form['results']['home_score']);
		unset($form['results']['away_score']);
	}
	$header = array(t(''), t('Home'), t('Away'));
	$output = theme('table', $header, $attributes);
	$output .= drupal_render($form);
	return $output;
}

function theme_leaguesite_matches_create($form){
	/**
	 * This function is used when listing matches which are being created. It arranges the matches into a table for nice formatting.
	 */
	$count = 0;
	$matchrows = array();
	while(isset($form['time'.$count])){
		//fields are time, matchday, home and away
		if(is_array($form['home'.$count])){
			 
			//check to see if matchday is in the table.
			if($form['matchday'.$count]['#type'] == 'hidden'){
				$matchrows[] = array(
				drupal_render($form['time'.$count]),
				drupal_render($form['home'.$count]),
				drupal_render($form['away'.$count]),
				);
			}else{
				$matchrows[] = array(
				drupal_render($form['time'.$count]),
				drupal_render($form['matchday'.$count]),
				drupal_render($form['home'.$count]),
				drupal_render($form['away'.$count]),
				);
			}
			unset($form['matches']['time'.$count]);
			unset($form['matches']['home'.$count]);
			unset($form['matches']['away'.$count]);
			unset($form['matches']['matchday'.$count]);
		}
		++$count;
	}
	//unset($form['matchcount']);
	if(count($matchrows) > 0){
		if(count($matchrows[0]) == 3){
			$header = array(t('Date'), t('Home'), t('Away'));
		}else{
			$header = array(t('Date'), t('Matchday'), t('Home'), t('Away'));
		}
		$output = theme('table', $header, $matchrows);
		$form['matches'] = array('#type' => 'markup', '#weight' => -1, '#value' => $output);
	}
	return drupal_render($form);
}

function leaguesite_match_score_edit_submit($form, $form_state){
	/**
	 * If there are additional match attributes for the game, I will have to save them first.
	 * I can then use the information to calculate the final score.
	 * Get the submitted results, load the node and change the result, then call node_save()
	 */
	$node = arg(1);
	$node = node_load(intval($node));
	//check for attributes, firstly if the leaguesite_scoring module is enabled. If there are any, they will start with a zero.

	if(module_exists('leaguesite_scoring') && isset($form_state['values']['attid0'])){
		//save the attributes
		$attributes = array();
		$count = 0;
		while(isset($form_state['values']['attid'.$count])){
			$attributes['home_team'][$form_state['values']['attid'.$count]] = $form_state['values']['homeattval'.$count];
			$attributes['away_team'][$form_state['values']['attid'.$count]] = $form_state['values']['awayattval'.$count];
			++$count;
		}
		$node->match_attributes = $attributes;
		leaguesite_scoring_calculate_score($node);
	}else{
		$node->home_score = (string)($form_state['values']['home_score']);
		$node->away_score = (string)($form_state['values']['away_score']);
	}
	node_save($node);

	if($form_state['clicked_button']['#value'] == t('Save and enter breakdown')){
		//load the changes to get if it is a result for the breakdown
		if($node->is_result){
			drupal_goto('node/'.$node->nid.'/edit/breakdown');
		}else{
			drupal_set_message(t('You cannot enter a match breakdown until the result is entered'));
			drupal_goto('node/'.$node->nid.'/edit/score');
		}
	}else{
		drupal_goto('node/'.$node->nid);
	}
}

function leaguesite_match_filter(){
	$session = &$_SESSION['leaguesite_match_overview_filter'];
	$session = is_array($session) ? $session : array();
	//get seasons

	$leagues_db = db_query("SELECT name, lid FROM {leaguesite_league} ORDER BY name ASC;");
	$leagues = array('-1' => t('Friendlies'));
	$leagues = array('0' => 'all');
	while($league = db_fetch_array($leagues_db)){
		$leagues[$league['lid']] =  $league['name'];
	}
	$seasons_db = db_query("SELECT name, sid FROM {leaguesite_season} WHERE archive = %d ORDER BY end desc;", 0);
	$seasons = array('0' => 'all');
	while($season = db_fetch_array($seasons_db)){
		$seasons[$season['sid']] =  $season['name'];
	}
	$matchdays_db = db_query('SELECT MAX(weeks) AS weeks FROM {leaguesite_league};');
	$matchday_count = db_fetch_array($matchdays_db);
	if($matchday_count['weeks'] > 0){
		$matchdays = array(0 => 'all');
		for($count = 0; $count < $matchday_count['weeks']; ++$count){
			$matchdays[] = $count+1;
		}
	}
	$tense = array(t('all'), t('past'), t('upcoming'));
	$status = array(t('all'), t('result pending'), t('result entered'));
	//get leagues
	//let user choose all, past or future matches
	$form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter Matches'),
    '#collapsible' => 'yes',
	/*'#theme' => 'leaguesite_match_filters',*/
	);

	$form['filters']['tense'] = array(
    '#title' => t('Match Date'),
    '#type' => 'select',
    '#options' => $tense,
    '#default_value' => $session['tense'],
	);
	$form['filters']['season'] = array(
    '#title' => t('Season'),
    '#type' => 'select',
    '#options' => $seasons,
    '#default_value' => $session['season'],
	);
	$form['filters']['league'] = array(
    '#title' => t('League'),
    '#type' => 'select',
    '#options' => $leagues,
    '#default_value' => $session['league'],
	);
	if($matchday_count['weeks'] > 0){
		$form['filters']['matchday'] = array(
      '#title' => t('Matchday'),
      '#type' => 'select',
      '#options' => $matchdays,
      '#default_value' => $session['matchday'],
		);
	}
	if(!isset($session['status'])){
		$session['status'] = 1;
	}
	$form['filters']['status'] = array(
    '#title' => t('Match Status'),
    '#type' => 'select',
    '#options' => $status,
    '#default_value' => $session['status'],
	);


	$form['filters']['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
	);
	$form['filters']['buttons']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
	);
	return $form;

}

/*
 * Leaguesite_match_filter_submit creates the session to filter the values.
 */
function leaguesite_match_filter_submit($form, $form_state){
	switch($form_state['values']['op']){
		case t('Reset'):
			$_SESSION['leaguesite_match_overview_filter'] = array();
			break;
		case t('Filter'):
			$_SESSION['leaguesite_match_overview_filter']['season'] = $form_state['values']['season'];
			$_SESSION['leaguesite_match_overview_filter']['league'] = $form_state['values']['league'];
			$_SESSION['leaguesite_match_overview_filter']['tense'] = $form_state['values']['tense'];
			$_SESSION['leaguesite_match_overview_filter']['status'] = $form_state['values']['status'];
			$_SESSION['leaguesite_match_overview_filter']['matchday'] = $form_state['values']['matchday'];
			break;
	}
}

function leaguesite_match_list(){
	//add a filter table here?
	$session = &$_SESSION['leaguesite_match_overview_filter'];
	$session = is_array($session) ? $session : array();
	//if tense is equal to zero, its set to all.
	if(isset($session['tense']) && $session['tense'] != 0){
		//1 is past, 2 is future
		switch($session['tense']){
			case 1:
				$filter = 'AND {leaguesite_match}.match_time < UNIX_TIMESTAMP() ';
				break;
			case 2:
				$filter = 'AND {leaguesite_match}.match_time > UNIX_TIMESTAMP() ';
				break;
		}
	}

	if(isset($session['season']) && $session['season'] != 0 && intval($session['season'])){
		//search for the season id
		$filter .= 'AND {leaguesite_relation}.sid = '.$session['season'].' ';
	}
	if(isset($session['league']) && $session['league'] != 0 && intval($session['league'])){
		//search for the league id
		$filter .= 'AND {leaguesite_relation}.lid = '.$session['league'].' ';
	}
	if(isset($session['status']) && $session['status'] != 0 && intval($session['status'])){
		//search for the status of the game
		if($session['status'] == 1){
			$filter .= 'AND {leaguesite_match}.is_result = 0 ';
		}else if($session['status'] == 2){
			$filter .= 'AND {leaguesite_match}.is_result = 1 ';
		}
	}
	if(isset($session['matchday']) && $session['matchday'] != 0 && intval($session['matchday'])){
		//search for the matchday of the game
		$filter .= 'AND {leaguesite_match}.matchday = '.$session['matchday'].' ';
	}


	//Here, I should add all the matches which can be edited by the current user. I would like to do this on a filter table though.
	$form = array();
	//set up form where we can change various match settings
	//get all the matches that have passed the fixture date with no score.
	$result = db_query("SELECT {leaguesite_match}.nid FROM {leaguesite_match} LEFT JOIN {leaguesite_relation} ON {leaguesite_match}.relation_id = {leaguesite_relation}.relation_id LEFT JOIN {leaguesite_season} ON {leaguesite_relation}.sid = {leaguesite_season}.sid WHERE {leaguesite_season}.archive = 0 ".$filter."ORDER BY {leaguesite_match}.match_time, {leaguesite_match}.nid;");
	//loop through the matches and check permissions to edit the matches. If so, add them to the form.
	/*
	* Check permissions using node_access('amend', node_load($nid , $revision = NULL, $reset = NULL)))
	*/
	//add a count to see how many matches we have added.
	$count = 0;
	while($match = db_fetch_array($result)){
		$node = node_load($match['nid']);
		if(node_access('update', $node)){
			$form[$count] = array(
        '#type' => 'fieldset',
        '#title' => t($node->title),
			);
			$form[$count][$count] = array(
        '#type' => 'hidden',
        '#value' => $node->title,
			);
			$match_day = date_make_date($node->match_time, 'UTC', DATE_ISO);
			$match_day = date_format_date($match_day, 'short');


			$form[$count]['match_date'.$count] = array(
        '#type' => 'hidden',
        '#value' => $match_day,
			);

			$form[$count]['match_id'.$count] = array(
        '#type' => 'hidden',
        '#value' => $node->nid,
			);
			$form[$count]['home'.$count] = array(
        '#type' => 'textfield',
        '#size' => 4,
        '#required' => FALSE,
        '#default_value' => $node->home_score,
			);
			$form[$count]['away'.$count] = array(
        '#type' => 'textfield',
        '#size' => 4,
        '#required' => FALSE,
        '#default_value' => $node->away_score,
			);
			++$count;
		}
	}
	if($count > 0){
		$form['submit'] = array(
      '#type' => 'submit', '#value' => t('Save Changes'), '#weight' => 10,
		);
	}
	return $form;
}

function leaguesite_match_list_validate($form, $form_state){

	for($id = 0; isset($form_state['values']['match_id'.$id]); ++$id){
		if(($form_state['values']['home'.$id] != '' && $form_state['values']['away'.$id] == '')||($form_state['values']['home'.$id] == '' && $form_state['values']['away'.$id] != '')){
			//return error - both teams must have scores if any have scores.
			form_set_error('home'.$id, t('Both teams must have a score entered if any have a score entered.'));
		}
	}
}

function leaguesite_match_list_submit($form, $form_state){
	for($id = 0; isset($form_state['values']['match_id'.$id]); ++$id){
		//set results
		$node = node_load($form_state['values']['match_id'.$id]);
		//dont resave if the match result hasn't changed.
		if($node->home_score == $form_state['values']['home'.$id] && $node->away_score == $form_state['values']['away'.$id]){
			continue;
		}
		$node->home_score = $form_state['values']['home'.$id];
		$node->away_score = $form_state['values']['away'.$id];
		node_save($node);
	}
	drupal_set_message($form_state['values'][$id].t('Results saved.'));
}

function leaguesite_match_settings(){
	$form = array();
	//set up form where we can change various match settings

	//add display options here
	$form['leaguesite_display_date'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display match date and time in node view'),
    '#default_value' => variable_get('leaguesite_match_display_date', TRUE),
	);

	$options = array(
    'd.m.Y.H:i' => 'dd.mm.yy hh:mm',
    'd.m.Y' => 'dd.mm.yy',
    'm.d.Y.H:i' => 'mm.dd.yy hh:mm',
    'm.d.Y' => 'mm.dd.yy',
	);
	$form['leaguesite_match_edit_date_format'] = array(
    '#type' => 'radios',
    '#title' => t('The format of the date input on the match edit form'),
    '#default_value' => variable_get('leaguesite_match_edit_date_format', 'd.m.Y.H:i'),
    '#options' => $options,
	);


	$form['leaguesite_display_scoring'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display the match scoring details in node view (requires leaguesite_scoring module)'),
    '#default_value' => variable_get('leaguesite_match_display_scoring', TRUE),
	);

	//allow user override of match naming
	$form['league_match_name'] = array(
    '#type' => 'textfield',
    '#title' => t('League match name pattern'),
    '#description' => t('This is the pattern used to name a match node for a league game. Values you can use are [league], [league_abbreviation], [season], [home_team] and [away_team]'),
    '#default_value' => variable_get('leaguesite_match_league_match_name', '[home_team] v [away_team]'),
	);

	$form['league_result_name'] = array(
    '#type' => 'textfield',
    '#title' => t('League result name pattern'),
    '#description' => t('This is the pattern used to name a match result node for a league game. Values you can use are [league], [league_abbreviation], [season], [home_team], [away_team], [home_score] and [away_score]'),
    '#default_value' => variable_get('leaguesite_match_league_result_name', '[home_score] [home_team] - [away_score] [away_team]'),
	);

	$form['friendly_match_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Friendly match name pattern'),
    '#description' => t('This is the pattern used to name a match node for a friendly game. Values you can use are [season], [home_team] and [away_team]'),
    '#default_value' => variable_get('leaguesite_match_friendly_match_name', '[home_team] v [away_team]'),
	);

	$form['friendly_result_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Friendly result name pattern'),
    '#description' => t('This is the pattern used to name a match result node for a friendly game. Values you can use are [season], [home_team], [away_team], [home_score] and [away_score]'),
    '#default_value' => variable_get('leaguesite_match_friendly_result_name', '[home_score] [home_team] - [away_score] [away_team]'),
	);

	if(module_exists('leaguesite_bracket')){
		$form['bracket_match_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Bracket match name pattern'),
      '#description' => t('This is the pattern used to name a match node for a bracket game. Values you can use are [season], [bracket], [home_team] and [away_team]'),
      '#default_value' => variable_get('leaguesite_match_bracket_match_name', '[home_team] v [away_team]'),
		);

		$form['bracket_result_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Bracket result name pattern'),
      '#description' => t('This is the pattern used to name a match result node for a bracket game. Values you can use are [season], [bracket], [home_team], [away_team], [home_score] and [away_score]'),
      '#default_value' => variable_get('leaguesite_match_bracket_result_name', '[home_score] [home_team] - [away_score] [away_team]'),
		);
	}

	//update league tables when a user enters a match result
	$form['update_table_insert'] = array(
    '#type' => 'checkbox',
    '#title' => t('Update league tables when match result is entered'),
    '#description' => t('If this is checked, when a user enters a match result, the corresponding league table will be updated'),
    '#default_value' => variable_get('leaguesite_match_update_table_insert', ''),
	);
	//update league tables when a user changes a match result
	$form['update_table_update'] = array(
    '#type' => 'checkbox',
    '#title' => t('Update league table when match result is amended'),
    '#default_value' => variable_get('leaguesite_match_update_table_update', ''),
	);
	//update league tables when a user deletes a match result
	$form['update_table_delete'] = array(
    '#type' => 'checkbox',
    '#title' => t('Update league table when match result is deleted'),
    '#default_value' => variable_get('leaguesite_match_update_table_delete', ''),
	);

	//change match teams once a match is created
	$form['change_teams'] = array(
    '#type' => 'checkbox',
    '#title' => t('Change teams'),
    '#description' => t('Allow a user to change the teams, and the league & season for a match once it has been created.'),
    '#default_value' => variable_get('leaguesite_match_change_teams', ''),
	);

	for($count = 0; $count < 24; ++$count){
		$hours[$count] = sprintf("%02d",$count); ;
	}
	for($count = 0; $count < 60; $count=$count+15){
		$mins[$count] = sprintf("%02d",$count);
	}
	$form['default_start'] = array(
    '#type' => 'fieldset',
    '#title' => t('The default start time for a match'),
	);
	$form['default_start']['hour'] = array(
    '#type' => 'select',
    '#title' => 'hour',
    '#required' => TRUE,
    '#options' => $hours,
    '#default_value' => variable_get('leaguesite_match_start_time_hour', '')
	);
	$form['default_start']['minute'] = array(
    '#type' => 'select',
    '#title' => 'minute',
    '#required' => TRUE,
    '#options' => $mins,
    '#default_value' => variable_get('leaguesite_match_start_time_minute', '')
	);
	$form['default_team'] = array(
    '#type' => 'textfield',
    '#title' => t('Default Team'),
    '#required' => FALSE,
    '#default_value' => leaguesite_match_get_team_name(variable_get('leaguesite_match_default_team', '')),
    '#autocomplete_path' => 'search/get_leaguesite_team',
	);

	//change match dates once a match is created (create a new node, set the original to postponed?)

	$form['submit'] = array(
    '#type' => 'submit', '#value' => t('Save')
	);
	return $form;
}

function leaguesite_match_settings_submit($form, $form_state){
	variable_set('leaguesite_match_league_result_name', $form_state['values']['league_result_name']);
	variable_set('leaguesite_match_league_match_name', $form_state['values']['league_match_name']);
	variable_set('leaguesite_match_update_table_insert', $form_state['values']['update_table_insert']);
	variable_set('leaguesite_match_update_table_update', $form_state['values']['update_table_update']);
	variable_set('leaguesite_match_update_table_delete', $form_state['values']['update_table_delete']);
	variable_set('leaguesite_match_change_teams', $form_state['values']['change_teams']);
	variable_set('leaguesite_match_start_time_hour', $form_state['values']['hour']);
	variable_set('leaguesite_match_start_time_minute', $form_state['values']['minute']);
	variable_set('leaguesite_match_friendly_result_name', $form_state['values']['friendly_result_name']);
	variable_set('leaguesite_match_friendly_match_name', $form_state['values']['friendly_match_name']);
	variable_set('leaguesite_match_display_date', $form_state['values']['leaguesite_display_date']);
	variable_set('leagusite_match_display_scoring', $form_state['values']['leaguesite_display_scoring']);
	variable_set('leaguesite_match_edit_date_format', $form_state['values']['leaguesite_match_edit_date_format']);
	if(module_exists('leaguesite_bracket')){
		variable_set('leaguesite_match_bracket_result_name', $form_state['values']['bracket_result_name']);
		variable_set('leaguesite_match_bracket_match_name', $form_state['values']['bracket_match_name']);
	}
	if($form_state['values']['default_team'] != ''){
		variable_set('leaguesite_match_default_team', leaguesite_match_get_team_id($form_state['values']['default_team']));
	}
}

/*
 * Theme for the match_admin panel, showing the upcoming games in rows
 */
function theme_leaguesite_match_list($form){
	if(module_exists('leaguesite_scoring')){
		$output = '<p>Click details if you want to enter the score breakdown of a match</p>';
	}
	if(isset($form['submit'])){
		$matches = array();
		foreach($form as $name => $element){
			if(isset($element['match_id'.$name]) && is_array($element['match_id'.$name])){
				//TODO: Add time and date to the match display
				$matches[] = array(
				check_plain($element['match_date'.$name]['#value']),
				check_plain($element[$name]['#value']),
				drupal_render($element['home'.$name]),
				drupal_render($element['away'.$name]),
				l('details', 'node/'.$element['match_id'.$name]['#value'].'/edit/score'),
				);
				unset($form[$name]);
			}
		}
		$teamsheader = array(t('Date & Time'), t('Match'), t('Home Score'), t('Away Score'), t('Edit'));
		if(count($matches) > 0){
			$output .= theme('table', $teamsheader, $matches);
		}else{
			//no teams found
			$output = '';
		}
		$output .= drupal_render($form);
	}else{
		$output = t('You have no outstanding matches!');
	}
	return $output;
}

/**
 * Form builder; Return form for user administration filters.
 *
 * @ingroup forms
 * @see user_filter_form_submit()
 */
function leaguesite_match_filter_form() {
	$session = &$_SESSION['user_overview_filter'];
	$session = is_array($session) ? $session : array();
	$filters = user_filters();

	$i = 0;
	$form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Show only users where'),
    '#theme' => 'user_filters',
	);
	foreach ($session as $filter) {
		list($type, $value) = $filter;
		// Merge an array of arrays into one if necessary.
		$options = $type == 'permission' ? call_user_func_array('array_merge', $filters[$type]['options']) : $filters[$type]['options'];
		$params = array('%property' => $filters[$type]['title'] , '%value' => $options[$value]);
		if ($i++ > 0) {
			$form['filters']['current'][] = array('#value' => t('<em>and</em> where <strong>%property</strong> is <strong>%value</strong>', $params));
		}
		else {
			$form['filters']['current'][] = array('#value' => t('<strong>%property</strong> is <strong>%value</strong>', $params));
		}
	}

	foreach ($filters as $key => $filter) {
		$names[$key] = $filter['title'];
		$form['filters']['status'][$key] = array(
      '#type' => 'select',
      '#options' => $filter['options'],
		);
	}

	$form['filters']['filter'] = array(
    '#type' => 'radios',
    '#options' => $names,
	);
	$form['filters']['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => (count($session) ? t('Refine') : t('Filter')),
	);
	if (count($session)) {
		$form['filters']['buttons']['undo'] = array(
      '#type' => 'submit',
      '#value' => t('Undo'),
		);
		$form['filters']['buttons']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
		);
	}

	drupal_add_js('misc/form.js', 'core');

	return $form;
}

/**
 * Process result from user administration filter form.
 */
function leaguesite_match_filter_form_submit($form, &$form_state) {
	$op = $form_state['values']['op'];
	$filters = user_filters();
	switch ($op) {
		case t('Filter'): case t('Refine'):
			if (isset($form_state['values']['filter'])) {
				$filter = $form_state['values']['filter'];
				// Merge an array of arrays into one if necessary.
				$options = $filter == 'permission' ? call_user_func_array('array_merge', $filters[$filter]['options']) : $filters[$filter]['options'];
				if (isset($options[$form_state['values'][$filter]])) {
					$_SESSION['user_overview_filter'][] = array($filter, $form_state['values'][$filter]);
				}
			}
			break;
		case t('Undo'):
			array_pop($_SESSION['user_overview_filter']);
			break;
		case t('Reset'):
			$_SESSION['user_overview_filter'] = array();
			break;
		case t('Update'):
			return;
	}

	$form_state['redirect'] = 'admin/user/user';
	return;
}

/**
 * Theme match administration filter selector.
 *
 * @ingroup themeable
 */
/*
 function theme_leaguesite_match_filters($form) {
 $output = '<ul class="clear-block">';
 if (!empty($form['current'])) {
 foreach (element_children($form['current']) as $key) {
 $output .= '<li>'. drupal_render($form['current'][$key]) .'</li>';
 }
 }

 $output .= '<li><dl class="multiselect">'. (!empty($form['current']) ? '<dt><em>'. t('and') .'</em> '. t('where') .'</dt>' : '') .'<dd class="a">';
 foreach (element_children($form['filter']) as $key) {
 $output .= drupal_render($form['filter'][$key]);
 }
 $output .= '</dd>';

 $output .= '<dt>'. t('is') .'</dt><dd class="b">';

 foreach (element_children($form['status']) as $key) {
 $output .= drupal_render($form['status'][$key]);
 }
 $output .= '</dd>';

 $output .= '</dl>';
 $output .= '<div class="container-inline" id="leaguesite_match-admin-buttons">'. drupal_render($form['buttons']) .'</div>';
 $output .= '</li></ul>';

 return $output;
 }
 */
/**
 * Menu callback for AHAH additions.
 */
function leaguesite_match_create_options_js() {
	//copied from the POLL module. I need to modify this for correct AHAH Drupal implementation
	module_load_include('inc', 'node', 'node.pages');
	$form_state = array('storage' => NULL, 'submitted' => FALSE);
	$form_build_id = $_POST['form_build_id'];
	// Get the form from the cache.
	$form = form_get_cache($form_build_id, $form_state);
	$args = $form['#parameters'];
	$form_id = array_shift($args);
	// We will run some of the submit handlers so we need to disable redirecting.
	$form['#redirect'] = FALSE;
	// We need to process the form, prepare for that by setting a few internals
	// variables.
	$form['#post'] = $_POST;
	$form['#programmed'] = FALSE;
	$form_state['post'] = $_POST;
	// Build, validate and if possible, submit the form.
	drupal_process_form($form_id, $form, $form_state);
	// This call recreates the form relying solely on the form_state that the
	// drupal_process_form set up.
	$form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
	// Render the new output.
	$choice_form = $form['matches'];
	unset($choice_form['#prefix'], $choice_form['#suffix']); // Prevent duplicate wrappers.
	$output = theme('status_messages').drupal_render($choice_form);
	drupal_json(array('status' => TRUE, 'data' => $output));
}

function leaguesite_create_matches_submit($form, &$form_state) {
	$form_selection = $form_state['values'];
	unset($form_state['submit_handlers']);
	form_execute_handlers('submit', $form, $form_state);
	$form_state['form_selection'] = $form_selection;
	$form_state['rebuild'] = TRUE;
}