<?php
function leaguesite_match_views_data(){
  $data['leaguesite_match']['table']['group'] = t('LeagueSite Match');
  $data['leaguesite_match']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  $data['leaguesite_match']['is_result'] = array(
    'title' => t('Is a result'),
    'help' => t('A boolean representing if a match has a result or not'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator'
    ),
  );
  $data['leaguesite_match']['home_team'] = array(
    'title' => t('Home team'),
    'help' => t('The nid of the home team in the match'),
    'relationship' => array(
      'base' => 'node',
      'base field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Home Team'),
    ),
  );
  $data['leaguesite_match']['away_team'] = array(
    'title' => t('Away team'),
    'help' => t('The nid of the away team in the match'),
    'relationship' => array(
      'base' => 'node',
      'base field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Away Team'),
    ),
  );
  $data['leaguesite_match']['home_score'] = array(
    'title' => t('Home score'),
    'help' => t('The home team score'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['leaguesite_match']['away_score'] = array(
    'title' => t('Away score'),
    'help' => t('The away team score'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['leaguesite_match']['matchday'] = array(
    'title' => t('Matchday'),
    'help' => t('The week of the league which the match takes place in'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'views_handler_argument_numeric',
    ),
  );
  $data['leaguesite_match']['match_time'] = array(
    'title' => t('Start time'),
    'help' => t('The starting time of the match'),
    'field' => array(
      'handler' => 'leaguesite_match_handler_match_time',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['leaguesite_match']['relation_id'] = array(
    'title' => t('League and season id'),
    'help' => t('The league and season the match belongs to'),
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['leaguesite_match']['team_id'] = array(
    'title' => t('Team Id'),
    'help' => t('The id of one of the teams playing.'),
    'argument' => array(
      'handler' => 'leaguesite_match_argument_team_id',
      'label' => t('Id for the team'),
    ),
    'filter' => array(
      'handler' => 'leaguesite_match_filter_team_id',
    ),
  );
  $data['leaguesite_team']['table']['group'] = t('LeagueSite Match');
  $data['leaguesite_team']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'team_id',
    ),
  );
  $data['leaguesite_team']['abbreviation'] = array(
    'title' => t('Team name abbreviation'),
    'help' => t('The short name of the team (if entered)'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  return $data;
}

function leaguesite_match_views_handlers(){
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'leaguesite_match') . '/views',
    ),
    'handlers' => array(
      'leaguesite_match_argument_team_id' => array(
        'parent' => 'views_handler_argument_numeric',
      ),
      'leaguesite_match_filter_team_id' => array(
        'parent' => 'views_handler_filter_numeric',
      ),
      'leaguesite_match_handler_match_time' => array(
        'parent' => 'views_handler_field_date',
      ),
    ),
  );
}