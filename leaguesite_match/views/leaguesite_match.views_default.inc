<?php

/**
 * @file
 * Default views for leaguesite_match.module.
 */

/**
 * Implementation of hook_views_default_views().
 */
function leaguesite_match_views_default_views() {
$view = new view;
$view->name = 'Match Calendar';
$view->description = 'List of upcoming matches';
$view->tag = '';
$view->view_php = '';
$view->base_table = 'node';
$view->is_cacheable = FALSE;
$view->api_version = 2;
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->override_option('relationships', array(
  'home_team' => array(
    'label' => 'Home Team',
    'required' => 0,
    'id' => 'home_team',
    'table' => 'leaguesite_match',
    'field' => 'home_team',
    'relationship' => 'none',
  ),
  'away_team' => array(
    'label' => 'Away Team',
    'required' => 0,
    'id' => 'away_team',
    'table' => 'leaguesite_match',
    'field' => 'away_team',
    'relationship' => 'none',
  ),
));
$handler->override_option('fields', array(
  'match_time' => array(
    'label' => '',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'target' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'html' => 0,
      'strip_tags' => 0,
    ),
    'empty' => '',
    'hide_empty' => 0,
    'empty_zero' => 0,
    'date_format' => 'custom',
    'custom_date_format' => 'd/m/y H:i',
    'exclude' => 0,
    'id' => 'match_time',
    'table' => 'leaguesite_match',
    'field' => 'match_time',
    'relationship' => 'none',
    'override' => array(
      'button' => 'Override',
    ),
  ),
  'title' => array(
    'label' => '',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'target' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'html' => 0,
      'strip_tags' => 0,
    ),
    'empty' => '',
    'hide_empty' => 0,
    'empty_zero' => 0,
    'link_to_node' => 0,
    'exclude' => 0,
    'id' => 'title',
    'table' => 'node',
    'field' => 'title',
    'relationship' => 'home_team',
  ),
  'nothing_1' => array(
    'label' => '',
    'alter' => array(
      'text' => ' - ',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'target' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'html' => 0,
      'strip_tags' => 0,
    ),
    'empty' => '',
    'hide_empty' => 0,
    'empty_zero' => 0,
    'exclude' => 0,
    'id' => 'nothing_1',
    'table' => 'views',
    'field' => 'nothing',
    'relationship' => 'none',
    'override' => array(
      'button' => 'Override',
    ),
  ),
  'title_1' => array(
    'label' => '',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'target' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'html' => 0,
      'strip_tags' => 0,
    ),
    'empty' => '',
    'hide_empty' => 0,
    'empty_zero' => 0,
    'link_to_node' => 0,
    'exclude' => 0,
    'id' => 'title_1',
    'table' => 'node',
    'field' => 'title',
    'relationship' => 'away_team',
  ),
));
$handler->override_option('sorts', array(
  'match_time' => array(
    'order' => 'ASC',
    'granularity' => 'minute',
    'id' => 'match_time',
    'table' => 'leaguesite_match',
    'field' => 'match_time',
    'relationship' => 'none',
  ),
));
$handler->override_option('filters', array(
  'status' => array(
    'operator' => '=',
    'value' => '1',
    'group' => '0',
    'exposed' => FALSE,
    'expose' => array(
      'operator' => FALSE,
      'label' => '',
    ),
    'id' => 'status',
    'table' => 'node',
    'field' => 'status',
    'relationship' => 'none',
  ),
  'is_result' => array(
    'operator' => '=',
    'value' => '0',
    'group' => '0',
    'exposed' => FALSE,
    'expose' => array(
      'operator' => FALSE,
      'label' => '',
    ),
    'id' => 'is_result',
    'table' => 'leaguesite_match',
    'field' => 'is_result',
    'override' => array(
      'button' => 'Overschrijf',
    ),
    'relationship' => 'none',
  ),
));
$handler->override_option('access', array(
  'type' => 'none',
));
$handler->override_option('cache', array(
  'type' => 'none',
));
$handler->override_option('title', 'Upcoming Match List');
$handler->override_option('empty', 'Sorry, no future matches found.');
$handler->override_option('empty_format', '1');
$handler->override_option('items_per_page', 0);
$handler->override_option('style_plugin', 'table');
$handler->override_option('style_options', array(
  'grouping' => '',
  'override' => 1,
  'sticky' => 0,
  'order' => 'asc',
  'columns' => array(
    'match_time' => 'match_time',
    'field_wedstrijd_type_value' => 'field_wedstrijd_type_value',
    'title' => 'title',
    'title_1' => 'title_1',
    'home_score' => 'home_score',
    'away_score' => 'away_score',
  ),
  'info' => array(
    'match_time' => array(
      'sortable' => 0,
      'separator' => '',
    ),
    'field_wedstrijd_type_value' => array(
      'sortable' => 0,
      'separator' => '',
    ),
    'title' => array(
      'sortable' => 0,
      'separator' => '',
    ),
    'title_1' => array(
      'sortable' => 0,
      'separator' => '',
    ),
    'home_score' => array(
      'sortable' => 0,
      'separator' => '',
    ),
    'away_score' => array(
      'sortable' => 0,
      'separator' => '',
    ),
  ),
  'default' => '-1',
));
$handler = $view->new_display('page', 'Page', 'page_2');
$handler->override_option('path', 'match_list');
$handler->override_option('menu', array(
  'type' => 'none',
  'title' => '',
  'description' => '',
  'weight' => 0,
  'name' => 'navigation',
));
$handler->override_option('tab_options', array(
  'type' => 'none',
  'title' => '',
  'description' => '',
  'weight' => 0,
  'name' => 'navigation',
));
	
  $views[$view->name] = $view;

	return $views;
}