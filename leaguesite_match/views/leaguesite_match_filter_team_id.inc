<?php
class leaguesite_match_filter_team_id extends views_handler_filter_numeric{
  function query(){
    //we need to make sure that the fields we need for the points difference are added to the query
    $this->ensure_my_table();
    $group = $this->query->set_where_group('OR', NULL, 'where');
    $this->query->add_where($group, 'leaguesite_match.home_team = '.$this->value['value']);
    $this->query->add_where($group, 'leaguesite_match.away_team = '.$this->value['value']);
    //$this->query->add_field($this->leaguesite_standings, $this->won);
  }
}