<?php
class leaguesite_match_handler_match_time extends views_handler_field_date{

  
  function render($values){
    $value = $values->{$this->field_alias};
    $format = $this->options['date_format'];
    $user_timezone_name = date_default_timezone_name(TRUE);
    $type = DATE_ISO;
    $value = date_make_date($value, $user_timezone_name, $type);
    
    
    if (in_array($format, array('custom', 'raw time ago', 'time ago', 'raw time span', 'time span'))) {
      $custom_format = $this->options['custom_date_format'];
    }
    
    if (!$value) {
      return theme('views_nodate');
    } else {
      switch ($format) {
        case 'custom':
        	return date_format_date($value, $format, $custom_format); 
        default:
          return date_format_date($value, $format);
      }
    }  
  }
  
  function options_form(&$form, $form_state){
  	parent::options_form($form, $form_state);
  	//remove the dates we don't render
  	unset($form['date_format']['#options']['raw time ago']);
  	unset($form['date_format']['#options']['time ago']);
    unset($form['date_format']['#options']['raw time span']);
    unset($form['date_format']['#options']['time span']);
    
    //return $form;
  }
}