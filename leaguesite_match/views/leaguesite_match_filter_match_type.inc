<?php
class leaguesite_match_filter_match_type extends views_handler_filter_in_operator{
	function get_value_options(){
    $options['friendly'] = t('Friendly Match');
    $options['league'] = t('League Match');
    $this->value_options = $options;
  }
}