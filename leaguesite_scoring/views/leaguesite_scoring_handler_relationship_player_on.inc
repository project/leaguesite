<?php
class leaguesite_scoring_handler_relationship_player_on extends views_handler_relationship{
  function query(){
  	parent::query();
    //we need to make sure that the fields we need for the points difference are added to the query
    $this->ensure_my_table();
    $group = $this->query->set_where_group('AND', NULL, 'where');
    $this->query->add_where($group, "leaguesite_event.event_type = 'sub'");
    //$this->query->add_field($this->leaguesite_standings, $this->won);
  }
}