<?php
class leaguesite_scoring_handler_field_event_method_name extends views_handler_field{
  function query(){
    //we need to make sure that the fields we need for the points difference are added to the query
    parent::query();
  	$this->ensure_my_table();
    $group = $this->query->set_where_group('AND', NULL, 'where');
    $this->query->add_where($group, "leaguesite_event.event_type = 'score'");
    //$this->query->add_field($this->leaguesite_standings, $this->won);
  }
}