<?php
function leaguesite_scoring_views_data(){
  $data = array();
  /**
   * Set up the data structure for views.
   */
  $data['leaguesite_match_attribute']['home_score_quantity'] = array(
    'title' => t('Home team method score'),
    'help' => t('The number of times the home team has scored using the method. See method name for the relevant name'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['leaguesite_match_attribute']['away_score_quantity'] = array(
    'title' => t('Away team method score'),
    'help' => t('The number of times the away team has scored using the method. See method name for the relevant name'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  //I now need to tell views how the tables link together.
  //first the match_id links to the nid of a game
  $data['leaguesite_match_attribute']['table']['group'] = t('LeagueSite Method');
  $data['leaguesite_match_attribute']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'match_id',
    ),
  );
  //leaguesite_method_0 is the orginial data we use for the join.
  $data['leaguesite_method_0']['table']['group'] = t('LeagueSite Method');
  $data['leaguesite_method_0']['table']['join'] = array(
    'node' => array(
      'left_table' => 'leaguesite_sport_method',
      'left_field' => 'method_id',
      'table' => 'leaguesite_method',
      'field' => 'method_id',
    ),
  );
  $data['leaguesite_method_0']['name'] = array(
    'title' => t('Scoring method'),
    'help' => t('The name of the scoring method'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['leaguesite_method_0']['points'] = array(
    'title' => t('Scoring method value'),
    'help' => t('The value of the scoring method'),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
  );
  $data['leaguesite_method_0']['weight'] = array(
    'title' => t('Scoring method weight'),
    'help' => t('The weight of the scoring method'),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
  );
  
  //leaguesite_method_1 is used for the leaguesite_event breakdown
  $data['leaguesite_method_1']['table']['group'] = t('LeagueSite Match Breakdown');
  $data['leaguesite_method_1']['table']['join'] = array(
    'node' => array(
      'left_table' => 'leaguesite_event',
      'left_field' => 'pen_score_id',
      'table' => 'leaguesite_method',
      'field' => 'method_id',
    ),
  );
  $data['leaguesite_method_1']['name'] = array(
    'title' => t('Scoring method'),
    'help' => t('The name of the scoring method'),
    'field' => array(
      'handler' => 'leaguesite_scoring_handler_field_event_method_name',
      'click sortable' => TRUE,
    ),
  );
  $data['leaguesite_method_1']['weight'] = array(
    'title' => t('Scoring method weight'),
    'help' => t('The weight of the scoring method'),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
  );
  
  
  $data['leaguesite_sport_method']['table']['group'] = t('LeagueSite Method');
  $data['leaguesite_sport_method']['table']['join'] = array(
    'node' => array(
      'left_table' => 'leaguesite_sport',
      'left_field' => 'score_id',
      'field' => 'score_id',
    ),
  );
  
  //now the team_id is a relationship link to a team nid
  $data['leaguesite_match_attribute']['method_id'] = array(
    'title' => t('Method name'),
    'help' => t('The name of the scoring method'),
    'relationship' => array(
      'base' => 'leaguesite_method_0',
      'base field' => 'method_id',
      'handler' => 'views_handler_relationship',
      'label' => t('Method name'),
    ),
  );
  
  //leaguesite_event
  $data['leaguesite_event']['table']['group'] = t('LeagueSite Match Breakdown');
  $data['leaguesite_event']['table']['join'] = array(
    'node' => array(
      'left_table' => 'leaguesite_match',
      'left_field' => 'nid',
      'field' => 'match_id',
    ),
  );
  
  $data['leaguesite_event']['time'] = array(
    'title' => t('Time'),
    'help' => t('The time that the penalty, substitution, or score occured'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  if(module_exists('leaguesite_player')){
  	$data['leaguesite_event']['player_id'] = array(
  	  'title' => t('Player'),
  	  'help' => t('The relation for the player involved in the event. For substitutions, this is the player going off.'),
  	  'relationship' => array(
        'base' => 'leaguesite_player',
        'base field' => 'player_id',
        'handler' => 'views_handler_relationship',
        'label' => t('Player'),
  	  ),  
  	);
  	$data['leaguesite_event']['player_on_id'] = array(
      'title' => t('Substitute player on'),
      'help' => t('The relation for the player coming on for a substitution.'),
      'relationship' => array(
        'base' => 'leaguesite_player',
        'base field' => 'player_id',
        'handler' => 'leaguesite_scoring_handler_relationship_player_on',
        'label' => t('Substitute player on'),
      ),
    );
  }
  
  
  //leaguesite_penalty
  $data['leaguesite_penalty']['table']['group'] = t('LeagueSite Match Breakdown');
  $data['leaguesite_penalty']['table']['join'] = array(
    'node' => array(
      'left_table' => 'leaguesite_event',
      'left_field' => 'pen_score_id',
      'field' => 'penalty_id', 
    ),
  );
  $data['leaguesite_penalty']['name'] = array(
    'title' => t('Penalty name'),
    'help' => t('The name of a penalty given to a team or player in a match'),
    'field' => array(
      'handler' => 'leaguesite_scoring_handler_field_penalty_name',
      'click sortable' => TRUE,
    ),
  );
  return $data;
}

//handlers for leaguesite_event
/*
 * I should have fields for event times, substitutions (player on, player off), scoring (player, method name) and penalties (player, penalty name)
 * 
 */

function leaguesite_scoring_views_handlers(){
	//the fields created by hook_views_data() will only need a field and a sort handler - No need for a filter.
	return array(
    'info' => array(
      'path' => drupal_get_path('module', 'leaguesite_scoring') . '/views',
    ),
    'handlers' => array(
      /*'leaguesite_scoring_handler_field_method_score' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'leaguesite_scoring_handler_sort_method_score' => array(
        'parent' => 'views_handler_sort',
      ),
      */
      'leaguesite_scoring_handler_field_penalty_name' => array(
        'parent' => 'views_handler_field',
      ),
      'leaguesite_scoring_handler_field_event_method_name' => array(
        'parent' => 'views_handler_field',
      ),
      'leaguesite_scoring_handler_relationship_player_on' => array(
        'parent' => 'views_handler_relationship', 
      ),
    ),
  );
}