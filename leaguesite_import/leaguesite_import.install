<?php

/**
 * @file 
 * Import and update leaguesite matches from a comma separated file (csv).
 */


/**
 * Implementation of hook_install()
 */
function leaguesite_import_install() {
  // Create tables.
  drupal_install_schema('leaguesite_import');
  return;
}

/**
* Implementation of hook_schema(). 
*/
function leaguesite_import_schema() {
  $schema['leaguesite_import'] = array(
     'description' => t("Settings for each import, and import setting templates."),
     'fields' => array(
        'import_id' => array(
        'description' => t("ID key of import or template."),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'disp-width' => '10'
      ),
      'name' => array(
        'description' => t("Label of import template, only used if this is an import template."),
        'type' => 'varchar',
        'length' => '25',
        'not null' => TRUE,
        'default' => ''
      ),
      'filename' => array( 
        'description' => t("Name of file being used as source of data for import."),
        'type' => 'varchar',
        'length' => '50',
        'not null' => TRUE,
        'default' => ''
      ),
      'oldfilename' => array( 
        'description' => t("Original name of file being used as source of data for import."), 
        'type' => 'varchar',
        'length' => '50',
        'not null' => TRUE,
        'default' => ''
      ),
      'filepath' => array( 
        'description' => t("Path to file being used as source of data for import."), 
        'type' => 'text',
        'size' => 'small',
        'not null' => TRUE
      ),
      'started' => array(  
        'description' => t("Datestamp of when import was started."), 
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'disp-width' => '11'
      ),
      'pointer' => array( 
        'description' => t("Pointer to where test/import last finished."), 
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'disp-width' => '10'
      ),
      'processed' => array(  
        'description' => t("Number of matches processed by import."), 
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'disp-width' => '10'
      ),
      'valid' => array(    
        'description' => t("Number of matches processed without errors."), 
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'disp-width' => '10'
      ),
      'first_line_skip' => array( 
        'description' => t("Ignore the first line of the CSV file, which are usually field headings."), 
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'disp-width' => '1'
      ),
      'field_match' => array( 
        'description' => t("Settings for how data matches to Drupal fields."), 
        'type' => 'text',
        'size' => 'big',
        'not null' => TRUE,
        'serialize' => TRUE
      ),
      'options' => array(   
        'description' => t("Store of all other options for import. Most of the other settings in this table will be moved into here in future."), 
        'type' => 'text',
        'size' => 'big',
        'not null' => TRUE,
        'serialize' => TRUE
      ),
      'setting' => array(    
        'description' => t("Status of import, or whether it is an import template."), 
        'type' => 'varchar',
        'length' => '10',
        'not null' => TRUE,
        'default' => ''
      )
    ),
    'primary key' => array('import_id'),
  );

  $schema['leaguesite_import_errors'] = array(  
    'description' => t("Record of errors encountered during an import."), 
    'fields' => array(    
      'import_id' => array( 
        'description' => t("ID key of import or template."), 
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'disp-width' => '10'
      ),
      'data' => array(  
        'description' => t("Data (matched to fields) for match that failed to import due to error."), 
        'type' => 'text',
        'size' => 'big',
        'not null' => TRUE,
        'serialize' => TRUE
      ),
      'errors' => array(  
        'description' => t("Error(s) encountered for match that failed to import."), 
        'type' => 'text',
        'size' => 'big',
        'not null' => TRUE,
        'serialize' => TRUE
      )
    ),
    'indexes' => array(
      'import_id' => array('import_id')
    ),
  );
  return $schema;
}

/**
* Implementation of hook_uninstall().
*/
function leaguesite_import_uninstall() {
  drupal_uninstall_schema('leaguesite_import');
  variable_del('leaguesite_import_max');
  variable_del('leaguesite_import_line_max');
  variable_del('leaguesite_import_settings');
}

