<?php

function theme_leaguesite_bracket_route_edit_form($form) {

  $output = drupal_render($form['head']);

  $wr = element_children($form['winner']);
  $lr = element_children($form['loser']);

  // if loser matches, generate header for winner matches
  if (count($lr) > 0) {
    $output .= '<p><h2>' . t('Winner Matches') . '</h2></p>';
  }

  $output .= "<!--winner_table_begin-->\n<table><tr>";

  $rtype = 'winner';

  foreach ($wr as $round) {

    $table = '<table>';

    $first = TRUE;

    foreach (element_children($form[$rtype][$round]) as $match) {

      // add a spacer between matches
      if ($first) {
        $first = FALSE;
      }
      else {
        $table .= '<tr><td>&nbsp;</td></tr>';
      }

      $hasloserroute = isset($form[$rtype][$round][$match]['loser_match']);

      $table .= "\n<!--match_begin-->\n<tr><td";
      if ($hasloserroute) {
        $table .= ' rowspan="2"';
      }
      $table .= '>';
      $table.= drupal_render($form[$rtype][$round][$match]['match_date']);
      $table .= drupal_render($form[$rtype][$round][$match]['matchid']);
      unset($form[$rtype][$round][$match]['matchid']);

      $table .= '</td>';

      if (isset($form[$rtype][$round][$match]['winner_match'])) {

        $table .= '<td nowrap><strong>' . t('Winner') .'</strong></td>';

        $table .= '<td>';
        $table .= drupal_render($form[$rtype][$round][$match]['winner_match']);
        unset($form[$rtype][$round][$match]['winner_match']);
        $table .= '</td>';

        $table .= '<td>';
        $table .= drupal_render($form[$rtype][$round][$match]['winner_comp']);
        unset($form[$rtype][$round][$match]['winner_match']);
        $table .= '</td>';
      }

      if ($hasloserroute) {

        $table .= '</tr><tr><td nowrap><strong>' . t('Loser') .'</strong></td>';

        $table .= '<td>';
        $table .= drupal_render($form[$rtype][$round][$match]['loser_match']);
        unset($form[$rtype][$round][$match]['loser_match']);
        $table .= '</td>';

        $table .= '<td>';
        $table .= drupal_render($form[$rtype][$round][$match]['loser_comp']);
        unset($form[$rtype][$round][$match]['loser_match']);
        $table .= '</td>';
      }

      if (isset($form[$rtype][$round][$match]['winner_result']) ||
      isset($form[$rtype][$round][$match]['loser_result']) ||
      isset($form[$rtype][$round][$match]['winner_result'])) {

        if (isset($form[$rtype][$round][$match]['winner_result'])) {
          $table .= '</tr><tr><td colspan="4" nowrap>';
          $table .= drupal_render($form[$rtype][$round][$match]['winner_result']);
          unset($form[$rtype][$round][$match]['winner_result']);
          $table .= '</td>';
        }
        if (isset($form[$rtype][$round][$match]['loser_result'])) {
          $table .= '</tr><tr><td colspan="4" nowrap>';
          $table .= drupal_render($form[$rtype][$round][$match]['loser_result']);
          unset($form[$rtype][$round][$match]['loser_result']);
          $table .= '</td></tr>';
        }
        if (isset($form[$rtype][$round][$match]['win_use_result'])) {
          $table .= '</tr><tr><td colspan="4" nowrap>';
          $table .= drupal_render($form[$rtype][$round][$match]['win_use_result']);
          unset($form[$rtype][$round][$match]['win_use_result']);
          $table .= '</td>';
          $table .= '</tr><tr><td colspan="4">';
          $table .= drupal_render($form[$rtype][$round][$match]['win_use_result_comment']);
          unset($form[$rtype][$round][$match]['win_use_result_comment']);
          $table .= '</td>';
        }
      }

      $table .= "</tr>\n<!--match_end-->\n";
    }   // end - foreach (element_children($form[$rtype][$round]) as $match)...

    $table .= '</table>';

    // Create the table inside the fieldset
    $form[$rtype][$round]['table'] = array(
      '#value' => $table
    );

    $output .= "\n<!--round_begin-->\n<td>";
    $output .= drupal_render($form[$rtype][$round]);
    $output .= "</td>\n<!--round_end-->\n";

  }   // end - foreach ($wm as $round)...

  $output .= "</tr></table>\n<!--winner_table_end-->\n";

  if (count($lr) > 0) {

    $rtype = 'loser';

    // header for loser matches
    $output .= '<hr /><p><h2>' .t('Loser Matches') . '</h2></p>';

    $output .= "\n<!--loser_table_begin-->\n<table><tr>";

    foreach ($lr as $round) {

      $table = '<table>';

      $first = TRUE;

      foreach (element_children($form[$rtype][$round]) as $match) {

        // add a spacer between matches
        if ($first) {
          $first = FALSE;
        }
        else {
          $table .= '<tr><td>&nbsp;</td></tr>';
        }

        $table .= "\n<!--match_begin-->\n<tr><td>";
        $table.= drupal_render($form[$rtype][$round][$match]['match_date']);
        $table .= drupal_render($form[$rtype][$round][$match]['matchid']);
        unset($form[$rtype][$round][$match]['matchid']);

        $table .= '</td>';

        if (isset($form[$rtype][$round][$match]['winner_match'])) {

          $table .= '<td nowrap><strong>' . t('Winner') .'</strong></td>';

          $table .= '<td>';
          $table .= drupal_render($form[$rtype][$round][$match]['winner_match']);
          unset($form[$rtype][$round][$match]['winner_match']);
          $table .= '</td>';

          $table .= '<td>';
          $table .= drupal_render($form[$rtype][$round][$match]['winner_comp']);
          unset($form[$rtype][$round][$match]['winner_match']);
          $table .= '</td>';
        }

        if (isset($form[$rtype][$round][$match]['winner_result']) ||
        isset($form[$rtype][$round][$match]['loser_result']) ||
        isset($form[$rtype][$round][$match]['winner_result'])) {

          if (isset($form[$rtype][$round][$match]['winner_result'])) {
            $table .= '</tr><tr><td colspan="4" nowrap>';
            $table .= drupal_render($form[$rtype][$round][$match]['winner_result']);
            unset($form[$rtype][$round][$match]['winner_result']);
            $table .= '</td>';
          }
          if (isset($form[$rtype][$round][$match]['loser_result'])) {
            $table .= '</tr><tr><td colspan="4" nowrap>';
            $table .= drupal_render($form[$rtype][$round][$match]['loser_result']);
            unset($form[$rtype][$round][$match]['loser_result']);
            $table .= '</td></tr>';
          }
          if (isset($form[$rtype][$round][$match]['win_use_result'])) {
            $table .= '</tr><tr><td colspan="4" nowrap>';
            $table .= drupal_render($form[$rtype][$round][$match]['win_use_result']);
            unset($form[$rtype][$round][$match]['win_use_result']);
            $table .= '</td>';
            $table .= '</tr><tr><td colspan="4">';
            $table .= drupal_render($form[$rtype][$round][$match]['win_use_result_comment']);
            unset($form[$rtype][$round][$match]['win_use_result_comment']);
            $table .= '</td>';
          }
        }

        $table .= "</tr>\n<!--match_end-->\n";
      }

      $table .= '</table>';

      $form[$rtype][$round]['table'] = array(
        '#value' => $table
      );

      $output .= "\n<!--round_begin-->\n<td>";
      $output .= drupal_render($form[$rtype][$round]);
      $output .= "</td>\n<!--round_end-->\n";
    }

    $output .= "</tr></table>\n<!--loser_table_end-->\n";

  }   // end - if (count($lr) > 0)...

  $output .= drupal_render($form);
  return $output;
}