<?php
class leaguesite_handler_field_played_away extends views_handler_field_numeric{
	function construct(){
		parent::construct();
		$this->additional_fields['won_away'] = 'won_away';
    $this->additional_fields['lost_away'] = 'lost_away';
    $this->additional_fields['drawn_away'] = 'drawn_away';

	}
	
	function query(){
		$this->ensure_my_table();
		$this->add_additional_fields();
  }
  
  function render($values){
  	$played_away = $values->leaguesite_standings_won_away + $values->leaguesite_standings_lost_away + $values->leaguesite_standings_drawn_away;

  	return $played_away;
  }
}