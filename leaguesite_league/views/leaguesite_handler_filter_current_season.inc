<?php
class leaguesite_handler_filter_current_season extends views_handler_filter{
  function query(){
  	$seasons = db_query('SELECT * FROM {leaguesite_season} ORDER BY start DESC');
  	//we loop through going further into the past looking for a season that has started. Once we find one, we filter to that season.
  	switch($this->value){
      case 'past':
  	    while($season = db_fetch_array($seasons)){
          //if the season has started, this is the most current one.
  	  	  if($season['start'] < time()){
            $season_id = $season['sid'];
            break;
  	      }
  	    }
  	  break;
  	  case 'future':
  		  while($season = db_fetch_array($seasons)){
  			  //we want a season that hasn't ended yet.
  			  if($season['end'] > time()){
  			  	$season_id = $season['sid'];
  			  }
  	    }
  	  break;
  	}
  	
    //we need to get the most recent season here and add it to the filter.
    $this->ensure_my_table();
    $group = $this->query->set_where_group('AND', NULL, 'where');
    $this->query->add_where($group, "{leaguesite_season}.sid = $season_id");
    //$this->query->add_field($this->leaguesite_standings, $this->won);
  }
  function value_form(&$form, &$form_state) {
    // Give the user options for specifiying how the current season is defined.
    $options = array(
      'future' => t('Choose the next season to start, regardless of its start date.'),
      'past' => t('Keep displaying the most recent season until the new season starts.'),
    );
    $form['value'] = array(
      '#type' => 'select',
      '#title' => t('Select what you want to happen when your current season ends'),
      '#options' => $options,
      '#default_value' => $this->value,
    );
    return $form;
  }
}