<?php
class leaguesite_handler_field_goals_for extends views_handler_field_numeric{
	function construct(){
		parent::construct();
		$this->additional_fields['goals_for_home'] = 'goals_for_home';
    $this->additional_fields['goals_for_away'] = 'goals_for_away';
	}
	
	function query(){
		$this->ensure_my_table();
		$this->add_additional_fields();
  }
  
  function render($values){
  	$for = $values->leaguesite_standings_for_home + $values->leaguesite_standings_for_away;
  	return $for;
  }
}