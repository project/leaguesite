<?php
class leaguesite_handler_field_points_difference_home extends views_handler_field_numeric{
	function query(){
    //we need to make sure that the fields we need for the points difference are added to the query
    $this->ensure_my_table();
    $this->add_additional_fields();
	}
	function construct(){
    parent::construct();
    $this->additional_fields['goals_for_home'] = 'goals_for_home';
    $this->additional_fields['goals_against_home'] = 'goals_against_home';
  }
  
  function render($values){
  	$difference = $values->leaguesite_standings_goals_for_home - $values->leaguesite_standings_goals_against_home;
  	//for some reason, this comes out as blank when a zero is returned. Not a massive problem, however as you can just use the empty text to display a zero
  	return $difference;
  }
}