<?php
class leaguesite_handler_filter_league_name extends views_handler_filter_in_operator{
	 
	function get_value_options(){
		$leagues = db_query("SELECT name FROM {leaguesite_league};");
		$this -> value_title = t('League name');
    
		while($league = db_fetch_array($leagues)){
		  $options[$league['name']] = $league['name'];
		}
		$this->value_options = $options;
	}
}