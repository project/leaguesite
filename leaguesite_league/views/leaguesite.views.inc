<?php
function leaguesite_views_data(){
  $data['leaguesite_standings']['table']['group'] = t('LeagueSite Standings');
  $data['leaguesite_standings']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'team_id',
    ),
  );
  $data['leaguesite_standings']['team_id'] = array(
    'title' => t('Team id'),
    'help' => t('The nid of the team that the standings belong to'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['leaguesite_standings']['played'] = array(
    'title' => t('Games Played'),
    'help' => t('Number of games played in the league'),
    'field' => array(
      'handler' => 'leaguesite_handler_field_played',
      'click sortable' => FALSE,
    ),
    'sort' => array(
      'handler' => 'leaguesite_handler_sort_played',
    ),
  );
  $data['leaguesite_standings']['played_home'] = array(
    'title' => t('Games Played home'),
    'help' => t('Number of games played in the league (at home)'),
    'field' => array(
      'handler' => 'leaguesite_handler_field_played_home',
      'click sortable' => FALSE,
    ),
    'sort' => array(
      'handler' => 'leaguesite_handler_sort_played_home',
    ),
  );
  $data['leaguesite_standings']['played_away'] = array(
    'title' => t('Games Played away'),
    'help' => t('Number of games played in the league (away from home)'),
    'field' => array(
      'handler' => 'leaguesite_handler_field_played_away',
      'click sortable' => FALSE,
    ),
    'sort' => array(
      'handler' => 'leaguesite_handler_sort_played_away',
    ),
  );
  $data['leaguesite_standings']['won'] = array(
    'title' => t('Games won'),
    'help' => t('The number of games the team has won'),
    'field' => array(
      'handler' => 'leaguesite_handler_field_won',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'leaguesite_handler_sort_won',
    ),
  );
  $data['leaguesite_standings']['won_home'] = array(
    'title' => t('Games won home'),
    'help' => t('The number of games the team has won (at home)'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['leaguesite_standings']['won_away'] = array(
    'title' => t('Games won away'),
    'help' => t('The number of games the team has won (away from home)'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['leaguesite_standings']['lost_home'] = array(
    'title' => t('Games lost home'),
    'help' => t('The number of games the team has lost (at home)'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['leaguesite_standings']['lost_away'] = array(
    'title' => t('Games lost away'),
    'help' => t('The number of games the team has lost (away from home)'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['leaguesite_standings']['lost'] = array(
    'title' => t('Games lost'),
    'help' => t('The number of games the team has lost'),
    'field' => array(
      'handler' => 'leaguesite_handler_field_lost',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'leaguesite_handler_sort_lost',
    ),
  );
  $data['leaguesite_standings']['drawn'] = array(
    'title' => t('Games drawn'),
    'help' => t('The number of games the team has drawn'),
    'field' => array(
      'handler' => 'leaguesite_handler_field_drawn',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'leaguesite_handler_sort_drawn',
    ),
  );
  $data['leaguesite_standings']['drawn_home'] = array(
    'title' => t('Games drawn home'),
    'help' => t('The number of games the team has drawn (at home)'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['leaguesite_standings']['drawn_away'] = array(
    'title' => t('Games drawn away'),
    'help' => t('The number of games the team has drawn (away from home)'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['leaguesite_standings']['goals_for'] = array(
    'title' => t('Goals scored'),
    'help' => t('The goals scored by this team'),
    'field' => array(
      'handler' => 'leaguesite_handler_field_goals_for',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'leaguesite_handler_filter_goals_for',
    ),
    'sort' => array(
      'handler' => 'leaguesite_handler_sort_goals_for',
    ),
  );
  $data['leaguesite_standings']['goals_for_home'] = array(
    'title' => t('Goals scored home'),
    'help' => t('The goals scored by this team (at home)'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['leaguesite_standings']['goals_for_away'] = array(
    'title' => t('Goals scored away'),
    'help' => t('The goals scored by this team (away from home)'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['leaguesite_standings']['goals_against'] = array(
    'title' => t('Goals conceded'),
    'help' => t('The number of goals conceded by this team'),
    'field' => array(
      'handler' => 'leaguesite_handler_field_goals_against',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'leaguesite_handler_filter_goals_against',
    ),
    'sort' => array(
      'handler' => 'leaguesite_handler_sort_goals_against',
    ),
  );
  $data['leaguesite_standings']['goals_against_home'] = array(
    'title' => t('Goals conceded home'),
    'help' => t('The number of goals conceded by this team (at home)'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['leaguesite_standings']['goals_against_away'] = array(
    'title' => t('Goals conceded away'),
    'help' => t('The number of goals conceded by this team (away from home)'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['leaguesite_standings']['bonus'] = array(
    'title' => t('Bonus points'),
    'help' => t('The number of bonus points for this team'),
    'field' => array(
      'handler' => 'leaguesite_handler_field_bonus',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'leaguesite_handler_filter_bonus',
    ),
    'sort' => array(
      'handler' => 'leaguesite_handler_sort_bonus',
    ),
  );
  $data['leaguesite_standings']['bonus_home'] = array(
    'title' => t('Bonus points home'),
    'help' => t('The number of bonus points for this team (at home)'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['leaguesite_standings']['bonus_away'] = array(
    'title' => t('Bonus points away'),
    'help' => t('The number of bonus points for this team (away from home)'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['leaguesite_standings']['points'] = array(
    'title' => t('Points'),
    'help' => t('Points in the League'),
    'field' => array(
      'handler' => 'leaguesite_handler_field_points',
      'click sortable' => FALSE,
    ),
    'sort' =>array(
      'handler'=> 'leaguesite_handler_sort_points',
    ),
  );
  $data['leaguesite_standings']['points_home'] = array(
    'title' => t('Points home'),
    'help' => t('Points in the League (at home)'),
    'field' => array(
      'handler' => 'leaguesite_handler_field_points_home',
      'click sortable' => FALSE,
    ),
    'sort' =>array(
      'handler'=> 'leaguesite_handler_sort_points_home',
    ),
  );
  $data['leaguesite_standings']['points_away'] = array(
    'title' => t('Points away'),
    'help' => t('Points in the League (away from home)'),
    'field' => array(
      'handler' => 'leaguesite_handler_field_points_away',
      'click sortable' => FALSE,
    ),
    'sort' =>array(
      'handler'=> 'leaguesite_handler_sort_points_away',
    ),
  );
  $data['leaguesite_standings']['points_difference'] = array(
    'title' => t('Points Difference'),
    'help' => t('Points for minus points against'),
    'field' => array(
      'handler' => 'leaguesite_handler_field_points_difference',
      'click sortable' => FALSE,
    ),
    'sort' =>array(
      'handler'=> 'leaguesite_handler_sort_points_difference',
    ),
  );
  $data['leaguesite_standings']['points_difference_home'] = array(
    'title' => t('Points Difference home'),
    'help' => t('Points for minus points against (at home)'),
    'field' => array(
      'handler' => 'leaguesite_handler_field_points_difference_home',
      'click sortable' => FALSE,
    ),
    'sort' =>array(
      'handler'=> 'leaguesite_handler_sort_points_difference_home',
    ),
  );
  $data['leaguesite_standings']['points_difference_away'] = array(
    'title' => t('Points Difference away'),
    'help' => t('Points for minus points against (away from home)'),
    'field' => array(
      'handler' => 'leaguesite_handler_field_points_difference_away',
      'click sortable' => FALSE,
    ),
    'sort' =>array(
      'handler'=> 'leaguesite_handler_sort_points_difference_away',
    ),
  );
  
  
  $data['leaguesite_relation_league']['table']['join'] = array(
    'node' => array(
      'table' => 'leaguesite_relation',
      'left_table' => 'leaguesite_standings',
      'left_field' => 'relation_id',
      'field' => 'relation_id',
    ),
  );
  $data['leaguesite_relation_match']['table']['group'] = t('LeagueSite Match');
  if(module_exists('leaguesite_match')){
    $data['leaguesite_relation_match']['table']['join'] = array(
      'node' => array(
        'table' => 'leaguesite_relation',
        'left_table' => 'leaguesite_match',
        'left_field' => 'relation_id',
        'field' => 'relation_id',
      ),
    );
  
    $data['leaguesite_relation_match']['relation_type'] = array(
      'title' => t('Type of match'),
      'help' => t('The type of match this node represents'),
      'filter' => array(
        'handler' => 'leaguesite_filter_match_type',
      ),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
    );
  }
  
  
  $data['leaguesite_league']['table']['group'] = t('LeagueSite Standings');
  $data['leaguesite_league']['table']['join'] = array(
    'node' => array(
      'left_table' => 'leaguesite_relation_league',
      'left_field' => 'lid',
      'field' => 'lid',
    ),
  );
  
  $data['leaguesite_league']['lid'] = array(
    'title' => t('League ID'),
    'help' => t('The unique identifier of the league.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'label' => t('Id for the league'),
    ),
  );
  
  $data['leaguesite_league']['name'] = array(
    'title' => t('League name'),
    'help' => t('The name of the league'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'leaguesite_handler_filter_league_name',
      'label' => t('Is in the league')
    ),
  );
  $data['leaguesite_league']['abbreviation'] = array(
    'title' => t('League name abbreviation'),
    'help' => t('The short name of the league (if entered)'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['leaguesite_league']['weight'] = array(
    'title' => t('League weight'),
    'help' => t('The weight of the league'),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  //add the data for the league fields. Once we have done that we will add a field for the match connection.
  $data['leaguesite_season']['table']['group'] = t('Leaguesite');
  $data['leaguesite_season']['table']['join'] = array(
    'node' => array(
      'left_table' => 'leaguesite_relation_league',
      'left_field' => 'sid',
      'field' => 'sid',
    ),
  );
  $data['leaguesite_season']['sid'] = array(
    'title' => t('Season ID'),
    'help' => t('The unique identifier of the season for leagues and matches.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'label' => t('Id for the season'),
    ),
  );
  $data['leaguesite_season']['name'] = array(
    'title' => t('Season name'),
    'help' => t('The name of the season for leagues and matches'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'leaguesite_handler_filter_season_name',
    ),
  );
  $data['leaguesite_season']['latest_season'] = array(
    'title' => t('Current season'),
    'help' => t('Filter to the current season. This will select the current season, or the most recently ended season'),
    'filter' => array(
      'handler' => 'leaguesite_handler_filter_current_season',
    ), 
  );
  $data['leaguesite_season']['weight'] = array(
    'title' => t('Season weight'),
    'help' => t('The weight of the season'),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  
  //if leaguesite_match is enabled, we should add the fields for that to connect too.
  if(module_exists('leaguesite_match')){
  	$data['leaguesite_season_match']['table']['group'] = t('LeagueSite Match');
  	$data['leaguesite_season_match']['table']['join'] = array(
      'node' => array(
        'table' => 'leaguesite_season',
        'left_table' => 'leaguesite_relation_match',
        'left_field' => 'sid',
        'field' => 'sid',
      ),
    );
    $data['leaguesite_season_match']['sid'] = array(
      'title' => t('Season ID'),
      'help' => t('The unique identifier of the season for a match.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
        'label' => t('Id for the season for matches'),
      ),
    );
    $data['leaguesite_season_match']['name'] = array(
      'title' => t('Season name'),
      'help' => t('The name of the season for matches'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'leaguesite_handler_filter_season_name',
      ),
    );
  }
    $data['leaguesite_sports']['table']['group'] = t('LeagueSite');
    $data['leaguesite_sports']['table']['join'] = array(
      'node' => array(
        'left_table' => 'leaguesite_league',
        'left_field' => 'sport_id',
        'field' => 'sport_id',
      ),
    );
    /*$data['leaguesite_sports']['name'] = array(
      'title' => t('Sport name'),
      'help' => t('The name of the sport that the match is for'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'leaguesite_handler_filter_season_name',
      ),
    );*/
  
  
  return $data;
}

function leaguesite_views_handlers(){

	return array(
	  'info' => array(
        'path' => drupal_get_path('module', 'leaguesite') . '/views',
      ),
	  'handlers' => array(
	    'leaguesite_handler_filter_league_name' => array(
	      'parent' => 'views_handler_filter_in_operator',
	    ),
	    'leaguesite_handler_filter_season_name' => array(
        'parent' => 'views_handler_filter_in_operator',
	    ),
	    'leaguesite_handler_field_points_difference' => array(
	      'parent' => 'views_handler_field_numeric',
	    ),
	    'leaguesite_handler_field_points_difference_home' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'leaguesite_handler_field_points_difference_away' => array(
        'parent' => 'views_handler_field_numeric',
      ),
	    'leaguesite_handler_field_points' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'leaguesite_handler_field_points_home' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'leaguesite_handler_field_points_away' => array(
        'parent' => 'views_handler_field_numeric',
      ),
	    'leaguesite_handler_field_played' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'leaguesite_handler_field_played_home' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'leaguesite_handler_field_played_away' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'leaguesite_handler_field_won' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'leaguesite_handler_field_drawn' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'leaguesite_handler_field_lost' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'leaguesite_handler_field_goals_for' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'leaguesite_handler_field_goals_against' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'leaguesite_handler_sort_played' => array(
        'parent' => 'views_handler_sort',
      ),
      'leaguesite_handler_sort_played_home' => array(
        'parent' => 'views_handler_sort',
      ),
      'leaguesite_handler_sort_played_away' => array(
        'parent' => 'views_handler_sort',
      ),
      'leaguesite_handler_sort_won' => array(
        'parent' => 'views_handler_sort',
      ),
      'leaguesite_handler_sort_drawn' => array(
        'parent' => 'views_handler_sort',
      ),
      'leaguesite_handler_sort_lost' => array(
        'parent' => 'views_handler_sort',
      ),
      'leaguesite_handler_sort_goals_for' => array(
        'parent' => 'views_handler_sort',
      ),
      'leaguesite_handler_sort_goals_against' => array(
        'parent' => 'views_handler_sort',
      ),
      'leaguesite_handler_sort_points_difference' => array(
        'parent' => 'views_handler_sort',
      ),
      'leaguesite_handler_sort_points_difference_home' => array(
        'parent' => 'views_handler_sort',
      ),
      'leaguesite_handler_sort_points_difference_away' => array(
        'parent' => 'views_handler_sort',
      ),
      'leaguesite_handler_sort_points' => array(
        'parent' => 'views_handler_sort',
      ),
      'leaguesite_handler_sort_points_home' => array(
        'parent' => 'views_handler_sort',
      ),
      'leaguesite_handler_sort_points_away' => array(
        'parent' => 'views_handler_sort',
      ),
      'leaguesite_filter_match_type' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
      'leaguesite_handler_filter_current_season' => array(
        'parent' => 'views_handler_filter',
      ),
	  ),
	);
}

/*
 * Below is an example of overriding the views_join object.
class leaguesite_join_relation extends views_join{
  function construct($table = NULL, $left_table = NULL, $left_field = NULL, $field = NULL, $extra = array(), $type = 'LEFT'){
    parent::construct($table, $left_table, $left_field, $field, $extra, $type);
    
    //parent::construct('leaguesite_relation', 'leaguesite_match', 'relation_id', 'relation_id', array(), 'LEFT');
  }
  
  function join($table, $query){
  	$output = parent::join($table, $query);
  	return $output;
  }
  
}*/