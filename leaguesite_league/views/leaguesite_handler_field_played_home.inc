<?php
class leaguesite_handler_field_played_home extends views_handler_field_numeric{
	function construct(){
		parent::construct();
		$this->additional_fields['won_home'] = 'won_home';
    $this->additional_fields['lost_home'] = 'lost_home';
    $this->additional_fields['drawn_home'] = 'drawn_home';

	}
	
	function query(){
		$this->ensure_my_table();
		$this->add_additional_fields();
  }
  
  function render($values){
  	$played_home = $values->leaguesite_standings_won_home + $values->leaguesite_standings_lost_home + $values->leaguesite_standings_drawn_home;

  	return $played_home;
  }
}