<?php
class leaguesite_handler_sort_won extends views_handler_sort{
	function query(){
    $this->query->add_field(NULL, 'leaguesite_standings.won_home + leaguesite_standings.won_away', 'leaguesite_standings_won', NULL);
    $this->query->add_orderby(NULL, NULL, $this->options['order'], 'leaguesite_standings_won');
	}
}