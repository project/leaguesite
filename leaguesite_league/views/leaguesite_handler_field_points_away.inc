<?php
class leaguesite_handler_field_points extends views_handler_field_numeric{
	function construct(){
		parent::construct();
		$this->additional_fields['won_away'] = 'won_away';
    $this->additional_fields['lost_away'] = 'lost_away';
    $this->additional_fields['drawn_away'] = 'drawn_away';
    $this->additional_fields['bonus_away'] = 'bonus_away';
    $this->additional_fields['win'] = array(
      'table' => 'leaguesite_sports',
      'field' => 'win',
    );
    $this->additional_fields['draw'] = array(
      'table' => 'leaguesite_sports',
      'field' => 'draw',
    );
    $this->additional_fields['loss'] = array(
      'table' => 'leaguesite_sports',
      'field' => 'loss',
    );
	}
	
	function query(){
		$this->ensure_my_table();
		$this->add_additional_fields();
    //we need to make sure that the fields we need for the games played are added to the query
  }
  
  function render($values){
  	$played = ($values->leaguesite_standings_won_away * $values->leaguesite_sports_win) + ($values->leaguesite_standings_lost_away * $values->leaguesite_sports_loss) + ($values->leaguesite_standings_drawn_away * $values->leaguesite_sports_draw) + $values->leaguesite_standings_bonus_away;
  	//for some reason, this comes out as blank when a zero is returned. Not a massive problem, however as you can just use the empty text to display a zero
  	return $played;
  }
}