<?php
class leaguesite_handler_field_drawn extends views_handler_field_numeric{
	function construct(){
		parent::construct();
		$this->additional_fields['drawn_home'] = 'drawn_home';
    $this->additional_fields['drawn_away'] = 'drawn_away';
	}
	
	function query(){
		$this->ensure_my_table();
		$this->add_additional_fields();
  }
  
  function render($values){
  	$drawn = $values->leaguesite_standings_drawn_home + $values->leaguesite_standings_drawn_away;
  	return $drawn;
  }
}