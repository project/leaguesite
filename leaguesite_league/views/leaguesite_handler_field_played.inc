<?php
class leaguesite_handler_field_played extends views_handler_field_numeric{
	function construct(){
		parent::construct();
		$this->additional_fields['won_home'] = 'won_home';
    $this->additional_fields['lost_home'] = 'lost_home';
    $this->additional_fields['drawn_home'] = 'drawn_home';
    $this->additional_fields['won_away'] = 'won_away';
    $this->additional_fields['lost_away'] = 'lost_away';
    $this->additional_fields['drawn_away'] = 'drawn_away';
	}
	
	function query(){
		$this->ensure_my_table();
		$this->add_additional_fields();
    //we need to make sure that the fields we need for the games played are added to the query
    //$this->leaguesite_standings_won = $this->query->add_field($this->leaguesite_standings, $this->won);
    //$this->leaguesite_standings_lost = $this->query->add_field($this->leaguesite_standings, $this->lost);
		//$this->leaguesite_standings_drawn = $this->query->add_field($this->leaguesite_standings, $this->drawn);
  }
  
  function render($values){
  	$played = $values->leaguesite_standings_won_home + $values->leaguesite_standings_won_away + $values->leaguesite_standings_lost_home + $values->leaguesite_standings_lost_away + $values->leaguesite_standings_drawn_home + $values->leaguesite_standings_drawn_away;
  	//for some reason, this comes out as blank when a zero is returned. Not a massive problem, however as you can just use the empty text to display a zero
  	return $played;
  }
}