<?php
class leaguesite_handler_field_goals_against extends views_handler_field_numeric{
	function construct(){
		parent::construct();
		$this->additional_fields['goals_against_home'] = 'goals_against_home';
    $this->additional_fields['goals_against_away'] = 'goals_against_away';
	}
	
	function query(){
		$this->ensure_my_table();
		$this->add_additional_fields();
  }
  
  function render($values){
  	$for = $values->leaguesite_standings_against_home + $values->leaguesite_standings_against_away;
  	return $for;
  }
}