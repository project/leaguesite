<?php
class leaguesite_handler_field_lost extends views_handler_field_numeric{
	function construct(){
		parent::construct();
		$this->additional_fields['lost_home'] = 'lost_home';
    $this->additional_fields['lost_away'] = 'lost_away';
	}
	
	function query(){
		$this->ensure_my_table();
		$this->add_additional_fields();
  }
  
  function render($values){
  	$lost = $values->leaguesite_standings_lost_home + $values->leaguesite_standings_lost_away;
  	return $lost;
  }
}