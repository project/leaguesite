<?php
class leaguesite_handler_sort_goals_against extends views_handler_sort{
	function query(){
    $this->query->add_field(NULL, 'leaguesite_standings.goals_against_home + leaguesite_standings.goals_against_away', 'leaguesite_standings_goals_against', NULL);
    $this->query->add_orderby(NULL, NULL, $this->options['order'], 'leaguesite_standings_goals_against');
	}
}