<?php
class leaguesite_handler_sort_drawn extends views_handler_sort{
	function query(){
    $this->query->add_field(NULL, 'leaguesite_standings.drawn_home + leaguesite_standings.drawn_away', 'leaguesite_standings_drawn', NULL);
    $this->query->add_orderby(NULL, NULL, $this->options['order'], 'leaguesite_standings_drawn');
	}
}