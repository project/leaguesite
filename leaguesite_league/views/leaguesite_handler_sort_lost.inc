<?php
class leaguesite_handler_sort_lost extends views_handler_sort{
	function query(){
    $this->query->add_field(NULL, 'leaguesite_standings.lost_home + leaguesite_standings.lost_away', 'leaguesite_standings_lost', NULL);
    $this->query->add_orderby(NULL, NULL, $this->options['order'], 'leaguesite_standings_lost');
	}
}