<?php
class leaguesite_handler_filter_season_name extends views_handler_filter_in_operator{
	function get_value_options(){
    $seasons = db_query("SELECT name FROM {leaguesite_season};");
    $this-> value_title = t('Season name');

    while($season = db_fetch_array($seasons)){
      $options[$season['name']] = $season['name'];
    }
    $this->value_options = $options;
  }
}