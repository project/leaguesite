<?php
class leaguesite_handler_sort_points_difference_away extends views_handler_sort{
	function query(){
    $this->query->add_field(NULL, 'leaguesite_standings.goals_for_away - leaguesite_standings.goals_against_away', 'leaguesite_standings_points_difference_away', NULL);
    $this->query->add_orderby(NULL, NULL, $this->options['order'], 'leaguesite_standings_points_difference_away');
	}
	
}
