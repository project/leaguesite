<?php
class leaguesite_handler_field_won extends views_handler_field_numeric{
	function construct(){
		parent::construct();
		$this->additional_fields['won_home'] = 'won_home';
    $this->additional_fields['won_away'] = 'won_away';
	}
	
	function query(){
		$this->ensure_my_table();
		$this->add_additional_fields();
  }
  
  function render($values){
  	$won = $values->leaguesite_standings_won_home + $values->leaguesite_standings_won_away;
  	return $won;
  }
}