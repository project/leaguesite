<?php
class leaguesite_handler_sort_goals_for extends views_handler_sort{
	function query(){
    $this->query->add_field(NULL, 'leaguesite_standings.goals_for_home + leaguesite_standings.goals_for_away', 'leaguesite_standings_goals_for', NULL);
    $this->query->add_orderby(NULL, NULL, $this->options['order'], 'leaguesite_standings_goals_for');
	}
}