<?php
class leaguesite_handler_sort_points_difference_home extends views_handler_sort{
	function query(){
    $this->query->add_field(NULL, 'leaguesite_standings.goals_for_home - leaguesite_standings.goals_against_home', 'leaguesite_standings_points_difference_home', NULL);
    $this->query->add_orderby(NULL, NULL, $this->options['order'], 'leaguesite_standings_points_difference_home');
	}
	
}
