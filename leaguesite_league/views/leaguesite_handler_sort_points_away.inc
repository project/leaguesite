<?php
class leaguesite_handler_sort_points extends views_handler_sort{
	function query(){
    $this->query->add_field(NULL, 'won_away', 'leaguesite_standings_won_away', NULL);
    $this->query->add_field(NULL, 'lost_away', 'leaguesite_standings_lost_away', NULL);
    $this->query->add_field(NULL, 'drawn_away', 'leaguesite_standings_drawn_away', NULL);
    $this->query->add_field(NULL, 'bonus_away', 'leaguesite_standings_bonus_away', NULL);
    
    $this->query->add_field('leaguesite_sports', 'win', 'leaguesite_sports_win', NULL);
    $this->query->add_field('leaguesite_sports', 'loss', 'leaguesite_sports_loss', NULL);
    $this->query->add_field('leaguesite_sports', 'draw', 'leaguesite_sports_draw', NULL);
    
    $this->query->add_orderby(NULL, '((leaguesite_standings.won_away) * leaguesite_sports.win) + ((leaguesite_standings.lost_away) * leaguesite_sports.loss) + ((leaguesite_standings.drawn_away) * leaguesite_sports.draw) + (leaguesite_standings.bonus_away)', $this->options['order'], 'leaguesite_standings_points_sort');
	}
}