<?php
class leaguesite_handler_sort_played extends views_handler_sort{
	function query(){
    $this->query->add_field(NULL, 'leaguesite_standings.won_home + leaguesite_standings.lost_home + leaguesite_standings.drawn_home', 'leaguesite_standings_played_home', NULL);
    $this->query->add_orderby(NULL, NULL, $this->options['order'], 'leaguesite_standings_played_home');
	}
}