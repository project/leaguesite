<?php

/**
 * @file
 * Explanation of the Leaguesite hooks
 */

/**
 * Called when a season is deleted in Leaguesite.
 * This is usually subsequently followed by a call of hook_leaguesite_relation_delete,
 * as any related relations are deleted.
 * 
 * @param Integer $season_id - the ID of the season being deleted
 * 
 * @ingroup leaguesite_hooks
 */
function hook_leaguesite_season_delete($season_id){
	
}

/**
 * Called when a league is deleted in Leaguesite.
 * This is usually subsequently followed by a call of hook_leaguesite_relation_delete,
 * as any related relations are deleted.
 * 
 * @param Integer $league_id - the ID of the league being deleted
 * 
 * @ingroup leaguesite_hooks
 */
function hook_leaguesite_league_delete($league_id){
	
}

/**
 * Called when a relation is deleted - this is usually triggered by the deletion of a season or league.
 * Could also be triggered by the deletion of something tying into the module, like a bracket.
 * 
 * Use this hook to delete anything to do with the relation, such as matches.
 * @param $relation_id - The ID of the relation being deleted
 * @ingroup leaguesite_hooks
 */
function hook_leaguesite_relation_delete($relation_id){
	
}

/*
 * The hooks below could, technically be implemented using hook_nodeapi,
 * but this causes a problem, as when this is used, the match details in the
 * leaguesite_match table will have already been updated. For checking the previous save's version
 * of the match details, or something like this, it is essential we get access before leaguesite_match
 * is updated. It makes most sense for hook_leaguesite_update. You could easily argue that the other
 * hooks are implementable through hook_nodeapi, but to maintain consistency, I implemented them along
 * with hook_leaguesite_match_update.
 * Examples of their implementation are shown in the leaguesite_bonus_example module.
 */

/**
 * Called when a match is created
 * Use to insert any fields or check the status of the match (is_result, home_score, away_score)
 * @param Object $node - the node details of the match being inserted.
 * @ingroup leaguesite_match_hooks
 */
function hook_leaguesite_match_insert(&$node){
  
}

/**
 * Called when a match is deleted. Just before the match is removed from leaguesite_match
 * @param Object $node - the node details of the match being removed.
 * @ingroup leaguesite_match_hooks
 */
function hook_leaguesite_match_delete(&$node){
	
}

/**
 * Called when a match is updated. Just before the match is updated on leaguesite_match
 * You can use this to access both the new match details (through node) and the old match details (through leaguesite_match)
 * @param Object $node - the new node details of the match being updated.
 * @ingroup leaguesite_match_hooks
 */
function hook_leaguesite_match_update(&$node){
	
}