<?php

/**
 * @file
 * Administrative page callbacks for the standings view. Allows a user to drill through to current standings for a league, and directly alter them.
 */



/**
 *
 * @return form allowing the user to edit the results of the
 */
function leaguesite_standings_edit(){
  //show the standings for the selected season and league and allow the user to directly alter the table
  $season = arg(4);
  $league = arg(5);
  if(is_numeric($season) && is_numeric($league)){
    //get the season and league name to add to the braedcrumb and page title
    $info = db_fetch_array(db_query("SELECT {leaguesite_league}.name AS league, {leaguesite_season}.name AS season FROM {leaguesite_league} LEFT JOIN {leaguesite_relation} ON {leaguesite_league}.lid = {leaguesite_relation}.lid LEFT JOIN {leaguesite_season} ON {leaguesite_relation}.sid = {leaguesite_season}.sid WHERE {leaguesite_relation}.lid = %d AND {leaguesite_relation}.sid = %d", $league, $season));
    drupal_set_title(t('Editing standings for %league - %season', array('%league' => $info['league'], '%season' => $info['season'])));
    $breadcrumb = drupal_get_breadcrumb();
    $breadcrumb[] = l($info['season'], 'admin/content/leaguesite/'.$season.'/view');
    drupal_set_breadcrumb($breadcrumb);
    $teams = db_query("SELECT {leaguesite_standings}.*, {node}.title FROM {leaguesite_standings}
		  LEFT JOIN {leaguesite_relation} ON {leaguesite_standings}.relation_id = {leaguesite_relation}.relation_id
		  LEFT JOIN {node} ON {leaguesite_standings}.team_id = {node}.nid
		  LEFT JOIN {leaguesite_league} ON {leaguesite_relation}.lid = {leaguesite_league}.lid
		  LEFT JOIN {leaguesite_sports} ON {leaguesite_league}.sport_id = {leaguesite_sports}.sport_id
		  WHERE {leaguesite_relation}.sid = %d AND {leaguesite_relation}.lid = %d
		  AND {leaguesite_relation}.relation_type = 'league'
		  ORDER BY (({leaguesite_standings}.won_home + {leaguesite_standings}.won_home) * {leaguesite_sports}.win) + (({leaguesite_standings}.drawn_home + {leaguesite_standings}.drawn_away)*{leaguesite_sports}.draw) + (({leaguesite_standings}.lost_home + {leaguesite_standings}.lost_away)*{leaguesite_sports}.loss) + ({leaguesite_standings}.bonus_home + {leaguesite_standings}.bonus_away) DESC, ({leaguesite_standings}.goals_for_home + {leaguesite_standings}.goals_for_away) DESC", $season, $league
    );
    $team_ids = "";
    while($team = db_fetch_array($teams)){
      $form[$team['team_id']]['name'.$team['team_id']] = array('#value' => $team['title']);
      $form[$team['team_id']]['won_home'.$team['team_id']] = array('#type' => 'textfield', '#size'=> 2, '#default_value' =>$team['won_home']);
      $form[$team['team_id']]['drawn_home'.$team['team_id']] = array('#type' => 'textfield', '#size'=> 2, '#default_value' =>$team['drawn_home']);
      $form[$team['team_id']]['lost_home'.$team['team_id']] = array('#type' => 'textfield', '#size'=> 2, '#default_value' =>$team['lost_home']);
      $form[$team['team_id']]['for_home'.$team['team_id']] = array('#type' => 'textfield', '#size'=> 2, '#default_value' =>$team['goals_for_home']);
      $form[$team['team_id']]['against_home'.$team['team_id']] = array('#type' => 'textfield', '#size'=> 2, '#default_value' =>$team['goals_against_home']);
      $form[$team['team_id']]['bonus_home'.$team['team_id']] =array('#type' => 'textfield', '#size'=>2, '#default_value' => $team['bonus_home']);
      $form[$team['team_id']]['won_away'.$team['team_id']] = array('#type' => 'textfield', '#size'=> 2, '#default_value' =>$team['won_away']);
      $form[$team['team_id']]['drawn_away'.$team['team_id']] = array('#type' => 'textfield', '#size'=> 2, '#default_value' =>$team['drawn_away']);
      $form[$team['team_id']]['lost_away'.$team['team_id']] = array('#type' => 'textfield', '#size'=> 2, '#default_value' =>$team['lost_away']);
      $form[$team['team_id']]['for_away'.$team['team_id']] = array('#type' => 'textfield', '#size'=> 2, '#default_value' =>$team['goals_for_away']);
      $form[$team['team_id']]['against_away'.$team['team_id']] = array('#type' => 'textfield', '#size'=> 2, '#default_value' =>$team['goals_against_away']);
      $form[$team['team_id']]['bonus_away'.$team['team_id']] =array('#type' => 'textfield', '#size'=>2, '#default_value' => $team['bonus_away']);
      $team_ids .= ",".$team['team_id'];
      $relation = $team['relation_id'];
    }
    //check to see if some teams exist. If not, don't add the other items. We will check for these items during the theme.
    if(isset($form)){
      $form['team_ids'] = array('#type' => 'hidden', '#value' => $team_ids);
      $form['relation'] = array('#type' => 'hidden', '#value' => $relation);
      $form['submit'] = array(
        '#type' => 'submit', '#value' => t('Save Changes')
      );
    }
  }else{
    drupal_not_found();
  }
  return $form;
}

/**
 * Function to validate submission of standings_edit()
 */
function leaguesite_standings_edit_validate($form, $form_state){
  //all standings figures submitted should be numeric.
  foreach($form_state['values'] as $id => $value){
    //don't process the standard values submitted. Only deal with the w values.
    if($id != 'op' && $id != 'submit' && $id !='form_build_id' && $id !='form_token' && $id != 'form_id' && $id != 'team_ids'){
      if(!is_numeric($value)){
        form_set_error($id, t('You should only enter integers in these fields'));
      }
    }
  }
}


/**
 * Function to deal with the submission of the standings_edit form
 */
function leaguesite_standings_edit_submit($form, $form_state){
  //get all the valid team ids, then loop through them.
  $teams = (explode(",", $form_state['values']['team_ids']));
  //ignore the first value, it is always empty.
  for($id = 1; $id < count($teams); ++$id){
    db_query("UPDATE {leaguesite_standings} SET won_home = %d, drawn_home = %d, lost_home = %d, goals_for_home = %d, goals_against_home = %d, bonus_home = %d, won_away = %d, drawn_away = %d, lost_away = %d, goals_for_away = %d, goals_against_away = %d, bonus_away = %d WHERE team_id = %d AND relation_id = %d", $form_state['values']['won_home'.$teams[$id]], $form_state['values']['drawn_home'.$teams[$id]], $form_state['values']['lost_home'.$teams[$id]], $form_state['values']['for_home'.$teams[$id]], $form_state['values']['against_home'.$teams[$id]], $form_state['values']['bonus_home'.$teams[$id]], $form_state['values']['won_away'.$teams[$id]], $form_state['values']['drawn_away'.$teams[$id]], $form_state['values']['lost_away'.$teams[$id]], $form_state['values']['for_away'.$teams[$id]], $form_state['values']['against_away'.$teams[$id]], $form_state['values']['bonus_away'.$teams[$id]], $teams[$id], $form_state['values']['relation']);
  }
  drupal_set_message(t('Standings updated.'));
}

/**
 * Themes the form allowing a user to directly alter the won, lost, drawn, for and against values in a table
 */
function theme_leaguesite_standings_edit($form){
  if(isset($form['submit'])){
    $teamrows = array();
    foreach($form as $name => $element){
      if(isset($element['name'.$name]) && is_array($element['name'.$name])){
        $teamrows[] = array(
        check_plain($element['name'.$name]['#value']),
        drupal_render($element['won_home'.$name]),
        drupal_render($element['won_away'.$name]),
        drupal_render($element['drawn_home'.$name]),
        drupal_render($element['drawn_away'.$name]),
        drupal_render($element['lost_home'.$name]),
        drupal_render($element['lost_away'.$name]),
        drupal_render($element['for_home'.$name]),
        drupal_render($element['for_away'.$name]),
        drupal_render($element['against_home'.$name]),
        drupal_render($element['against_away'.$name]),
        drupal_render($element['bonus_home'.$name]),
        drupal_render($element['bonus_away'.$name]),
        );
        unset($form[$name]);
      }
    }
    $teamsheader = array(t('Team Name'), t('Won home'), t('Won away'), t('Draw home'), t('Draw away'), t('Lost home'), t('Lost away'), t('For home'), t('For away'), t('Against home'), t('Against away'), t('Bonus home'), t('Bonus away'));
    if(count($teamrows) > 0){
      $output = theme('table', $teamsheader, $teamrows);
    }else{
      //no teams found
      $output = '';
    }
    $output .= drupal_render($form);
  }else{
    $output = t('You have no teams added!');
  }
  return $output;
}

/**
 * Themes the view of the Standings admin table
 * @param unknown_type $form - to theme
 * @return Themed output
 */
function theme_leaguesite_standings_edit_season($form) {
  $seasonrows = array();
  foreach ($form as $name => $element) {
    if (isset($element['name']) && is_array($element['name'])) {
      $seasonrows[] = array(
      $element['name']['#value'],
      //check_plain($element['sid']['start']['#value']),
      //check_plain($element['sid']['end']['#value']),
      );
      unset($form[$name]);
    }
  }
  $seasonheader = array(t('Season Name'));
  if(count($seasonrows) > 0){
    $output = theme('table', $seasonheader, $seasonrows);
  }else{
    //TODO: The output needs sorting out when there are no seasons to show
    $output = "";
  }
  return $output;
}

/**
 * Themes the view of the Standings admin table
 * @param unknown_type $form - to theme
 * @return Themed output
 */
function theme_standings_edit_league($form) {
  //check to see there are teams.
  $leaguerows = array();
  foreach ($form as $name => $element) {
    if (isset($element['name']) && is_array($element['name'])) {
      $leaguerows[] = array(
      drupal_render($element['name']),
      //check_plain($element['sid']['start']['#value']),
      //check_plain($element['sid']['end']['#value']),
      );
      unset($form[$name]);
    }
  }
  $leagueheader = array(t('League Name'));
  if(count($leaguerows) > 0){
    $output = theme('table', $leagueheader, $leaguerows);
  }else{
    //TODO: The output needs sorting out when there are no seasons to show
    $output = '';
  }

  return $output;
}
