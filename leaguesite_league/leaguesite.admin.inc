<?php

/**
 * @file
 * Administrative page callbacks for the league module.
 */

/**
 * Admin form for the seasons page
 */
function leaguesite_admin(){
	//get the seasons from the database for the list display
	//TODO: Anywhere I display the date should be formatted to a custom format, can use get and set variable on a config form I need to create
	$seasons = db_query("SELECT * FROM {leaguesite_season} ORDER BY weight");
	$total_seasons = db_fetch_array(db_query('SELECT count(sid) AS sid FROM {leaguesite_season}'));
	$total_seasons = $total_seasons['sid'];
	$count = 0;
    $form['seasons']['#tree'] = true;
	while($row = db_fetch_array($seasons)){
	    $form['seasons'][$row['sid']]['weight'] = array('#type' => 'weight', '#delta' => $total_seasons, '#default_value' => $row['weight'], '#attributes' => array('class' => "season-weight"));
	    $form['seasons'][$row['sid']]['id'] = array('#type' => 'hidden', '#value' => $row['sid']);
		$form['seasons'][$row['sid']]['name'] = array('#value' => $row['name'],);
		$form['seasons'][$row['sid']]['start'] = array('#value' => date("d M y", $row['start']));
		$form['seasons'][$row['sid']]['end'] = array('#value' => date("d M y", $row['end']));
		$form['seasons'][$row['sid']]['view'] = array('#value' => l(t('view'), 'admin/content/leaguesite/'.$row['sid']));
		$form['seasons'][$row['sid']]['configure'] = array('#value' => l(t('configure'), 'admin/content/leaguesite/'.$row['sid'].'/edit'));
		$form['seasons'][$row['sid']]['delete'] = array('#value' => $default ? '' : l(t('delete'), 'admin/content/leaguesite/'.$row['sid'].'/delete'));
		++$count;
	}
	if($count == 0){
		drupal_set_message('You currently have no seasons setup. To get started, '.l('add a new season', 'admin/content/leaguesite/add'), 'warning');
	}else{
	  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
	}
	return $form;
}

function leaguesite_list(){
  $form = array();
  $form['leagues']['#tree'] = true;
	$leagues = db_query("SELECT * FROM {leaguesite_league} ORDER BY weight;");
	$league_count = db_fetch_array(db_query('SELECT count(lid) AS lid FROM {leaguesite_league}'));
	$league_count = $league_count['lid'];
	//TODO: sort out count of seasons in this view?
	$count = 0;
	while($row = db_fetch_array($leagues)){
	    $form['leagues'][$row['lid']]['weight'] = array('#type' => 'weight', '#delta' => $league_count, '#default_value' => $row['weight'], '#attributes' => array('class' => 'league-weight'));
		$form['leagues'][$row['lid']]['name'] = array('#value' => $row['name'],);
		$form['leagues'][$row['lid']]['weeks'] = array('#value' => $row['weeks']);
		$form['leagues'][$row['lid']]['configure'] = array('#value' => l(t('configure'), 'admin/content/leaguesite/leagues/'.$row['lid'].'/edit'));
		$form['leagues'][$row['lid']]['delete'] = array('#value' => $default ? '' : l(t('delete'), 'admin/content/leaguesite/leagues/'. $row['lid'].'/delete'));
		++$count;
	}
	if($count == 0){
		$form['notice'] = array('#value' => 'No leagues currently exist, please add one above.');
	}else{
      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
      );
	}
	return $form;
}

function leaguesite_list_submit($form, $form_state){
  foreach($form_state['values']['leagues'] as $lid => $weight){
    db_query('UPDATE {leaguesite_league} SET weight = %d WHERE lid = %d;', $weight['weight'], $lid);
  }
}

function leaguesite_admin_submit($form, $form_state){
  foreach($form_state['values']['seasons'] as $sid => $weight){
    db_query('UPDATE {leaguesite_season} SET weight = %d WHERE sid = %d;', $weight['weight'], $sid);
  }
}

/**
 * Add season form for the league admin page
 */
function leaguesite_add(){

	$breadcrumb = drupal_get_breadcrumb();
	$breadcrumb[] = l(t('Leagues'), 'admin/content/leaguesite/leagues');
	drupal_set_breadcrumb($breadcrumb);

	//get the scoring details
	$result = db_query("SELECT * FROM {leaguesite_sports};");
	$scores[] = array();
	while($row = db_fetch_array($result)){
		$scores[$row['sport_id']] = t($row['name']);
	}
	//$scores['other'] = t('Other');
	$form = array();
	//add a new season
	$form['league_name'] = array(
    '#type' => 'textfield',
    '#title' => t('League Name'),
    '#description' => t('The name of your league'),
    '#size' => 30,
    '#maxlength' => 64,
    '#required' => TRUE,
	);
	$form['abbreviation'] = array(
    '#type' => 'textfield',
    '#title' => t('League Abbreviation'),
    '#description' => t('The shorthand name of your league, if you have one.'),
    '#size' => 30,
    '#maxlength' => 64,
    '#required' => FALSE,
  );
	$form['league_weeks'] = array(
    '#type' => 'textfield',
    '#title' => t('Match Weeks'),  
    '#description' => t('Set the number of weeks you play matches for over the course of the season. Default value is 10.'),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => FALSE,
	);
	$form['league_type'] = array(
    '#type' => 'select',
    '#title' => t('Scoring type used in league'),
    '#options' => $scores,
    '#description' => t('Choose a scoring type, if the suitable one exists. You can change it later. If you cannot find the one you want, you can add a scoring type.'),
	);
	$form['submit'] = array(
    '#type' => 'submit', '#value' => t('Create League')
	);
	return $form;
}

function leaguesite_edit(){
	//get the season and the details for the form
	$league = arg(4);
	if(is_numeric($league)){

		//get the scoring details
		$result = db_query("SELECT * FROM {leaguesite_sports};");
		$scores[] = array();
		while($row = db_fetch_array($result)){
			$scores[$row['sport_id']] = t($row['name']);
		}
		 
		$breadcrumb = drupal_get_breadcrumb();
		$breadcrumb[] = l(t('Leagues'), 'admin/content/leaguesite/leagues');
		drupal_set_breadcrumb($breadcrumb);
		 
		$result = db_fetch_array(db_query("SELECT {leaguesite_league}.*, {leaguesite_sports}.sport_id FROM {leaguesite_league}, {leaguesite_sports} WHERE {leaguesite_league}.lid = %d AND {leaguesite_league}.sport_id = {leaguesite_sports}.sport_id", $league));
		$form = array();
		//add a new league form
		$form['league_name'] = array(
      '#type' => 'textfield',
      '#title' => t('League Name'),
      '#description' => t('The name of your league'),
      '#size' => 30,
      '#maxlength' => 50,
      '#required' => TRUE,
      '#default_value' => $result['name'],
		);
		$form['abbreviation'] = array(
    '#type' => 'textfield',
    '#title' => t('League Abbreviation'),
    '#description' => t('The shorthand name of your league, if you have one.'),
    '#size' => 30,
    '#maxlength' => 64,
    '#required' => FALSE,
		'#default_value' => $result['abbreviation'],
  );
		$form['league_id'] = array(
      '#type' => 'hidden',
      '#value' => $league,
		);
		$form['league_weeks'] = array(
      '#type' => 'textfield',
      '#title' => t('Match Weeks'),  
      '#description' => t('The number of weeks you play matches for over the course of the season. Default value is 10.'),
      '#size' => 3,
      '#maxlength' => 3,
      '#required' => FALSE,
      '#default_value' => $result['weeks'],
		);

		$form['league_type'] = array(
      '#type' => 'select',
      '#title' => t('Type of League'),
      '#default_value' => $result['sport_id'],
      '#options' => $scores,
      '#description' => t('Choose a league type, if the suitable one exists. This will automatically set up the points scoring system which can be edited later if needs be.'),
		);

		$form['submit'] = array(
      '#type' => 'submit', '#value' => t('Amend League')
		);
		return $form;
	}else{
		drupal_not_found();
	}
}

/**
 * Function to deal with deleting a selected season
 */
function leaguesite_delete_league(){
	$league = arg(4);
	$league = db_fetch_array(db_query('SELECT * FROM {leaguesite_league} WHERE lid = %d;', $league));

	if ($league) {
		$form['league'] = array('#type' => 'hidden', '#value' => $league['lid']);
		$form['name'] = array('#type' => 'hidden', '#value' => $league['name']);
		return confirm_form($form, t('Are you sure you want to delete the league %league?', array('%league' => $league['name'])), 'admin/content/leaguesite/leagues', t('Deleting this league will not remove teams in it, but all the table standings will be lost.'), t('Delete'), t('Cancel'));
	}
	else {
		drupal_not_found();
	}
}

/**
 * Function to validate the edit form before saving the changes
 * @param $form
 * @param $form_state
 */
function leaguesite_add_validate($form, $form_state){
	//Check the name of the league name
	$result = db_fetch_array(db_query("SELECT name FROM {leaguesite_league} WHERE name = '%s'", mysql_escape_string($form_state['values']['league_name'])));

	//TODO: Add a link to edit the league they match?
	if($result){
		form_set_error('league_name', t('You already have a league with this name. Please choose a different name. Or you could edit this league.'));
	}
}

/**
 * Function to deal with the submission of the create/edit league form
 */
function leaguesite_add_submit($form, $form_state){
	//Get the values entered, making sure they are valid, then enter them into the database.
	$name = check_plain($form_state['values']['league_name']);
	$abbreviation = check_plain($form_state['values']['abbreviation']);
	$weeks = intval($form_state['values']['league_weeks']);
	$scoring = intval($form_state['values']['league_type']);
	db_query("INSERT INTO {leaguesite_league} (name, weeks, sport_id, abbreviation) VALUES('%s', %d, %d, '%s');", $name, $weeks, $scoring, $abbreviation);
	drupal_set_message(t('Added league %league.', array('%league' => $name)));
}

/**
 * Function to delete selected league
 */
function leaguesite_delete_league_submit($form, $form_state){
	//Remove the league from the table. In the future, also will have to delete all standings related to the season too
	/* TODO: I should develop an API for deleting these things.
	 * Perhaps through a hook. I think all the matches in a league that has been deleted should be changed to friendlies?
	 * API call for delete league, delete relation, delete season.
	 * It should keep a nice watch on what needs to be notified,
	 * so that data is always up to date. */
	$league_id = $form_state['values']['league'];
	
	module_invoke_all('leaguesite_league_delete', $league_id);
  $relations = db_query('SELECT relation_id FROM {leaguesite_relation} WHERE lid = %d', $league_id);
  while($row = db_fetch_array($relations)){
    _leaguesite_relation_delete(intval($row['relation_id']));
  }
	if(is_numeric($league_id) && $league_id > -1){
	  db_query("DELETE FROM {leaguesite_league} WHERE lid = %d", $league_id);
	  db_query('DELETE FROM {leaguesite_relation} WHERE lid = %d', $league_id);
	  drupal_set_message(t('League deleted.'));
	}
	
	drupal_goto('admin/content/leaguesite');
}

/**
 * Function to save changes made to a season
 * @param $form
 * @param $form_state
 */
function leaguesite_edit_submit($form, $form_state){
	$name = check_plain($form_state['values']['league_name']);
	$abbreviation = check_plain($form_state['values']['abbreviation']);
	$weeks = intval($form_state['values']['league_weeks']);
	$sport_id = intval($form_state['values']['league_type']);
	$lid = intval($form_state['values']['league_id']);
	db_query("UPDATE {leaguesite_league} SET name = '%s', weeks = %d, sport_id = %d, abbreviation = '%s' WHERE lid = %d;", $name, $weeks, $sport_id, $abbreviation, $lid);
	drupal_set_message(t('Changes Saved'));
}


/**
 * Function to show a leagues details and allow a user to add teams to the league.
 * @return $form - the form for the editing of the league.
 */
function leaguesite_view(){
	/*Show the details of the league, then a list of checkboxes for each team.
	 * Tick the ones currently in the league, and allow the user to tick and untick before submitting
	 */
	$season = arg(4);
	$league = arg(5);
	if(is_numeric($league) && is_numeric($season)){
		//get the league details.
		$result = db_fetch_array(db_query("SELECT {leaguesite_league}.name AS leaguename, {leaguesite_league}.weeks, {leaguesite_season}.name FROM {leaguesite_league} LEFT JOIN {leaguesite_relation} ON {leaguesite_league}.lid = {leaguesite_relation}.lid LEFT JOIN {leaguesite_season} ON {leaguesite_relation}.sid = {leaguesite_season}.sid WHERE {leaguesite_relation}.lid = %d AND {leaguesite_relation}.sid=%d;", $league, $season));
		drupal_set_title(t('Viewing league %league', array('%league' => $result['leaguename'])));
		$breadcrumb = drupal_get_breadcrumb();
		$breadcrumb[] = l($result['name'], 'admin/content/leaguesite/'.$season.'/view');
		drupal_set_breadcrumb($breadcrumb);
		if($result){
			//get the details
			$weeks = $result['weeks'];
			//TODO: The layout of these labels should be sorted out!!
			$form['start'] = array('#value' => t('Weeks: ').$weeks);
			$form['teams'] = array(
        '#type' => 'fieldset',
        '#title' => t('Teams in the league for '.$result['name']),
        '#description' => t('Note, if you remove any of these teams from the league after the start of a season, the matches stay intact, but they lose their standing in the table.'),
			);
			//list the teams available, like the season view, where I can tick the teams involved in the league.
			$all_teams = db_query("SELECT nid, title FROM {node} WHERE type = 'leaguesite_team' AND status = 1 ORDER BY nid;");
			$enabled_teams_result = db_query("SELECT {node}.nid FROM {node} LEFT JOIN {leaguesite_standings} ON {node}.nid = {leaguesite_standings}.team_id LEFT JOIN {leaguesite_relation} ON {leaguesite_standings}.relation_id = {leaguesite_relation}.relation_id WHERE {node}.type = 'leaguesite_team' AND {leaguesite_relation}.lid = %d AND {leaguesite_relation}.sid = %d ORDER BY {node}.nid;", $league, $season);
			$enabled_teams = db_fetch_array($enabled_teams_result);
			$count = 0;
			while($row = db_fetch_array($all_teams)){
				++$count;
				if($enabled_teams['nid'] == $row['nid']){
					//the current row is present in the league.
					$ticked = TRUE;
					//get the next enabled one
					$enabled_teams = db_fetch_array($enabled_teams_result);
				}else{
					$ticked = FALSE;
				}
				$form['teams'][$row['nid']] = array(
          '#type' => 'checkbox',
          '#title' => $row['title'],
          '#default_value' => $ticked,
				);
			}
			if($count == 0){
				$form['teams']['label'] = array(
          '#value' => t('No teams added yet, go to create content to add a team'),
				);
			}else{
				$form['submit'] = array(
          '#type' => 'submit', '#value' => t('Make Changes')
				);
			}

		}
	}else{
		drupal_not_found();
	}
	return $form;
}

/**
 * Function to deal with the submit of the league view - look through all the teams and add / remove standings information as appropriate
 */
function leaguesite_view_submit($form, $form_state){
	$season = arg(4);
	$league = arg(5);
	$result = db_fetch_array(db_query("SELECT relation_id FROM {leaguesite_relation} WHERE lid = %d AND sid = %d", $league, $season));
	$relation_id = $result['relation_id'];
	if(!is_numeric($season) || !is_numeric($league)){
		drupal_not_found();
	}else{
		//loop through the form league values and add/remove the league_relation as appropriate
		foreach($form_state['values'] as $id => $checked){
			//get the value from league_relation and check to see it matches the chosen state in the tickbox
			$result = db_fetch_array(db_query("SELECT {node}.nid FROM {node} LEFT JOIN {leaguesite_standings} ON {node}.nid = {leaguesite_standings}.team_id LEFT JOIN {leaguesite_relation} ON {leaguesite_standings}.relation_id = {leaguesite_relation}.relation_id WHERE {leaguesite_relation}.lid = %d AND {leaguesite_relation}.sid = %d AND {leaguesite_standings}.team_id = %d ORDER BY {node}.nid;", $league, $season, check_plain($id)));
			if(($result == FALSE && $checked == 1) || ($result != FALSE && $checked == 0)){
				if($checked == 1){
					//if checked equals 1, then add the relation
					db_query("INSERT INTO {leaguesite_standings} (relation_id,team_id) VALUES (%d, %d)", $relation_id, check_plain($id));
				}else if($checked == 0){
					//if checked equals 0, then delete the relation
					db_query("DELETE FROM {leaguesite_standings} WHERE team_id = %d AND relation_id = %d", check_plain($id), $relation_id);
				}
			}
		}
		drupal_set_message("Changes Saved");
	}
}

/**
 * Theme the admin overview form.
 *
 * @ingroup themeable
 */
function theme_leaguesite_admin($form) {
	$seasonrows = array();
	foreach ($form['seasons'] as $name => $element) {
		if(is_array($element['name'])){
			$seasonrows[] = array(
			  'data' => array(
			    
			    drupal_render($element['name']),
			    check_plain($element['start']['#value']),
			    check_plain($element['end']['#value']),
			    drupal_render($element['weight']),
			    drupal_render($element['view']),
			    drupal_render($element['configure']),
			    drupal_render($element['delete']),
			  ),
			  'class' => 'draggable',
			);
			unset($form['seasons'][$name]);
		}
	}
	$seasonheader = array(t('Season Name'), t('Starts'), t('Ends'), t('Weight'), array('data' => t('Operations'), 'colspan' => 3));
	if(count($seasonrows) > 0){
		$output = theme('table', $seasonheader, $seasonrows, array('id' => 'season-table'));
		drupal_add_tabledrag('season-table', 'order', 'sibling', 'season-weight');
	}
	$output .= drupal_render($form).'<br />';
	$output .= l('Add a new season', 'admin/content/leaguesite/add');
	$output .= '<p></p>';
	$result = leaguesite_check_image_field();
  if(isset($result)){
    $output .= "<div class='messages warning'>".$result.'</div>'; 
  }
	return $output;
}

/**
 * Theme the league list form
 */
    function theme_leaguesite_list($form) {
	$leaguerows = array();
	drupal_add_tabledrag('league-table', 'order' ,'sibling', 'league-weight');
	foreach ($form['leagues'] as $name => $element) {
		if(is_array($element['name'])){
			$leaguerows[] = array(
			'data' => array(
			
			drupal_render($element['name']),
			drupal_render($element['weight']),
			check_plain($element['weeks']['#value']),
			drupal_render($element['configure']),
			drupal_render($element['delete']),
			),
			'class' => 'draggable',
			);
			unset($form['leagues'][$name]);
		}
	}
	$leagueheader = array(t('League Name'), t('Weight'), t('Weeks'), array('data' => t('Operations'), 'colspan' => 2));
	if(count($leaguerows) > 0){
		$output = theme('table', $leagueheader, $leaguerows, array('id' => 'league-table'));
	}
	$output.= drupal_render($form);
	$output .= '<br />'.l('Add a new league', 'admin/content/leaguesite/leagues/add');
	return $output;
}

function leaguesite_add_image_field(){
	
	leaguesite_add_default_image_field();

  drupal_set_message(t('Default image support configured for Leaguesite Teams using CCK and ImageCache.'));

  drupal_goto('admin/content/leaguesite');
}

/**
 * This adds an imagefield to a team. Code taken from uc_product.module.. Shame on me.
 * Requires imagefield, filefield and CCK.
 */
function leaguesite_add_default_image_field() {
  module_load_include('inc', 'imagefield', 'imagefield_widget');
  module_load_include('inc', 'filefield', 'filefield_widget');
  module_load_include('inc', 'content', 'includes/content.crud');

  $label = t('Image');
  $field = array(
    'type_name' => 'leaguesite_team',
    'field_name' => 'field_image_cache',
    'label' => $label,
    'type' => 'filefield',
    'widget_type' => 'imagefield_widget',
    'weight' => -2,
    'file_extensions' => 'gif jpg png',
    'custom_alt' => 1,
    'custom_title' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => 1,
    'list_field' => '0',
    'list_default' => 1,
    'description_field' => '0',
    'module' => 'filefield',
    'widget_module' => 'imagefield',
    'columns' => array(
      'fid' => array(
        'type' => 'int',
        'not null' => FALSE,
      ),
      'list' => array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => FALSE,
      ),
      'data' => array(
        'type' => 'text',
        'serialize' => TRUE,
      ),
    ),
    'display_settings' => array(
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'hidden',
      ),
      'full' => array(
        'format' => 'leaguesite_full',
      ),
      4 => array(
        'format' => 'hidden',
      ),
    ),
  );

  $instances = content_field_instance_read(array('field_name' => 'field_image_cache', 'type_name' => 'leaguesite_team'));
  if (count($instances) < 1) {
    // Only add the field if it doesn't exist. Don't overwrite any changes.
    content_field_instance_create($field);
  }
}

function leaguesite_sports_autocomplete($string){
  $items = array();
  $result = db_query("SELECT name FROM {leaguesite_sports} WHERE name LIKE '%s%'", $string);
  while($obj = db_fetch_object($result)){
    $items[$obj->name] = check_plain($obj->name);
  }
  print drupal_to_js($items);
  exit();
}

function leaguesite_seasons_autocomplete($string){
  $items = array();
  $result = db_query("SELECT name FROM {leaguesite_season} WHERE name LIKE '%s%'", $string);
  while($obj = db_fetch_object($result)){
    $items[$obj->name] = check_plain($obj->name);
  }
  print drupal_to_js($items);
  exit();
}

function leaguesite_teams_autocomplete($string){
  $matches = array();
  if($string) {
    $result = db_query_range("SELECT {node}.title FROM {node} LEFT JOIN {leaguesite_team} ON {node}.nid = {leaguesite_team}.team_id WHERE {node}.type = 'leaguesite_team' AND ({node}.title LIKE '%s%%' OR {leaguesite_team}.abbreviation LIKE '%s%%')", array($string, $string), 0,     variable_get('keyword_autocomplete_matches', 10));
    while ($data = db_fetch_object($result)) {
      $matches[$data->title] = check_plain($data->title);
    }
  }
  drupal_json($matches);
}