<?php

/**
 * @file
 * Administrative page callbacks for the season view, delete and edit of the league module.
 */

/**
 * Add season form for the league admin page
 */
function leaguesite_season_add(){
  $form = array();
  //add a new season
  $form['season_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('The name of your season'),
    '#size' => 30,
    '#maxlength' => 50,
    '#required' => TRUE,
  );

  $form['season_startdate'] = array(
    '#type' => 'date',
    '#title' => t('Start Date'),  
    '#description' => t('Select when your season starts. If you do not know the exact day, set it sometime in the middle of the season break'),
    '#required' => TRUE,
  );
  $form['season_enddate'] = array(
    '#type' => 'date',
    '#title' => t('End Date'),  
    '#description' => t('Select when your season ends.'),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit', '#value' => t('Create Season')
  );
  return $form;
}

function leaguesite_season_edit(){
  //get the season and the details for the form
  $season = arg(3);
  if(is_numeric($season)){
    $result = db_fetch_array(db_query("SELECT * FROM {leaguesite_season} WHERE sid = %d", $season));
    $form = array();
    //edit the season
    $form['season_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t('The name of your season'),
      '#size' => 30,
      '#maxlength' => 64,
      '#required' => TRUE,
      '#default_value' => $result['name'],
    );

    $form['season_old_name'] = array(
      '#type' => 'hidden',
      '#value' => $result['name'],
    );
    $form['season_id'] = array(
      '#type' => 'hidden',
      '#value' => $season,
    );

    $form['season_startdate'] = array(
      '#type' => 'date',
      '#title' => t('Start Date'),  
      '#description' => t('Select when your season starts. If you do not know the exact day, set it sometime in the middle of the season break'),
      '#required' => TRUE,
      '#default_value' => array('year'=>date("Y",$result['start']), 'month'=>date("n",$result['start']), 'day'=>date("j",$result['start'])),
    );
    $form['season_enddate'] = array(
      '#type' => 'date',
      '#title' => t('End Date'),  
      '#description' => t('Select when your season ends.'),
      '#required' => TRUE,
      '#default_value' => array('year'=>date("Y",$result['end']), 'month'=>date("n",$result['end']), 'day'=>date("j",$result['end'])),
    );
    $state = array('0' => t('Active'), '1'=> t('Inactive'));
    $form['archive'] = array(
      '#type' => 'radios',
      '#options' => $state,
      '#title' => t('State'),
      '#description' => t('If the season is selected as inactive, then any matches or leagues in this season will not appear in any admin interfaces.'),
      '#required' => TRUE,
      '#default_value' => $result['archive'],
      '#weight' => -4,
    );
    $form['submit'] = array(
      '#type' => 'submit', '#value' => t('Save Changes')
    );
    return $form;
  }else{
    drupal_not_found();
  }
}

/**
 * Function to deal with deleting a selected season
 */
function leaguesite_season_delete(){
  $season = arg(3);
  $season = db_fetch_array(db_query('SELECT * FROM {leaguesite_season} WHERE sid = %d;', $season));

  if ($season) {
    $form['season'] = array('#type' => 'hidden', '#value' => $season['sid']);
    $form['name'] = array('#type' => 'hidden', '#value' => $season['name']);
    return confirm_form($form, t('Are you sure you want to delete the season %season?', array('%season' => $season['name'])), 'admin/content/leaguesite/', t('Deleting this season will not remove the leagues and teams, but all the table standings and matches will be lost.'), t('Delete'), t('Cancel'));
  }
  else {
    drupal_not_found();
  }
}

/**
 * Validate the season input - the end date has to be later than the start date
 * @return unknown_type
 */
function leaguesite_season_add_validate($form, $form_state){
  $result = db_fetch_array(db_query("SELECT name FROM {leaguesite_season} WHERE name = '%s'", mysql_escape_string($form_state['values']['season_name'])));
  if($result){
    form_set_error('season_name', t('You already have a season with this name. Please choose a different name'));
  }
  $start = $form_state['values']['season_startdate'];
  $end = $form_state['values']['season_enddate'];
  if((mktime(0, 0, 0, intval($start['month']), intval($start['day']), intval($start['year']))) >= (mktime(0, 0, 0, intval($end['month']), intval($end['day']), intval($end['year'])))){
    form_set_error('season_enddate', t('Your season finish time cannot be earlier than the end time, or be the same day'));
  }
}

/**
 * Function to validate the edit form before saving the changes
 * @param $form
 * @param $form_state
 */
function leaguesite_season_edit_validate($form, $form_state){
  //if the user has changed the name, check it does not equal another season name
  if($form_state['values']['season_name'] != $form_state['values']['season_old_name']){
    $result = db_fetch_array(db_query("SELECT name FROM {leaguesite_season} WHERE name = '%s'", mysql_escape_string($form_state['values']['season_name'])));

    if($result){
      form_set_error('season_name', t('You already have a season with this name. Please choose a different name'));
    }
  }
  $start = $form_state['values']['season_startdate'];
  $end = $form_state['values']['season_enddate'];
  if((mktime(0, 0, 0, intval($start['month']), intval($start['day']), intval($start['year']))) >= (mktime(0, 0, 0, intval($end['month']), intval($end['day']), intval($end['year'])))){
    form_set_error('season_enddate', t('Your season finish time cannot be earlier than the end time, or be the same day'));
  }
}

/**
 * Function to deal with the submission of the create/edit season form
 */
function leaguesite_season_add_submit($form, $form_state){
  //check submission, then add the season to the database. If update, we should find the edited season and change the settings.
  $name = check_plain($form_state['values']['season_name']);
  $start = mktime(0,0,0,$form_state['values']['season_startdate']['month'], $form_state['values']['season_startdate']['day'], $form_state['values']['season_startdate']['year']);
  $end = mktime(0,0,0,intval($form_state['values']['season_enddate']['month']), intval($form_state['values']['season_enddate']['day']), intval($form_state['values']['season_enddate']['year']));
  db_query("INSERT INTO {leaguesite_season} (name, start, end, archive) VALUES('%s', %d, %d, 0);", $name, $start, $end);
  drupal_set_message(t('Added season %season.', array('%season' => $name)));
  //We also need to add the friendlys for this season. Get the sports that need adding, and add to leaguesite_relation for them.
  $sports = db_query('SELECT sport_id FROM {leaguesite_sports}');
  $season_id = db_last_insert_id('{leaguesite_season}', 'sid');
  while($sport = db_fetch_array($sports)){
  	db_query("INSERT INTO {leaguesite_relation} (sport_id, sid, lid, relation_type) VALUES (%d, %d, %d, 'friendly');", $sport['sport_id'], $season_id, -1);
  }
}

/**
 * Function to delete selected season
 */
function leaguesite_season_delete_submit($form, $form_state){
  //remove the season from the table. In the future, also will have to delete all standings related to the season too
  
  $season_id = $form_state['values']['season'];
	module_invoke_all('leaguesite_season_delete', $season_id);
	//also need to delete all the relations from the database which this season is linked with.
	$relations = db_query('SELECT relation_id FROM {leaguesite_relation} WHERE sid = %d', $season_id);
	while($row = db_fetch_array($relations)){
		_leaguesite_relation_delete(intval($row['relation_id']));
	}  
	
  
	db_query("DELETE FROM {leaguesite_season} WHERE sid = %d", $season_id);
  drupal_set_message(t('Season deleted.'));
  drupal_goto('admin/content/leaguesite');
}

/**
 * Function to save changes made to a season
 * @param $form
 * @param $form_state
 */
function leaguesite_season_edit_submit($form, $form_state){
  $name = mysql_escape_string($form_state['values']['season_name']);
  $archive = intval($form_state['values']['archive']);
  $start = mktime(0,0,0,$form_state['values']['season_startdate']['month'], $form_state['values']['season_startdate']['day'], $form_state['values']['season_startdate']['year']);
  $end = mktime(0,0,0,intval($form_state['values']['season_enddate']['month']), intval($form_state['values']['season_enddate']['day']), intval($form_state['values']['season_enddate']['year']));
  $season = $form_state['values']['season_id'];
  db_query("UPDATE {leaguesite_season} SET name = '%s', start = %d, end = %d, archive = %d WHERE sid = %d;", $name, $start, $end, $archive, $season);
  drupal_set_message(t('Changes Saved'));
}


/**
 * Function to show a season details and allow a user to add leagues to the season.
 * @return $form - the form for the editing.
 */
function leaguesite_season_view(){
  /*
   * Show the details of the season, then a list of checkboxes for each league.
   * Tick the ones added currently, and allow the user to tick and untick before submitting
   */
  $season = arg(3);
  if(is_numeric($season)){
    $result = db_fetch_array(db_query("SELECT * FROM {leaguesite_season} WHERE sid = %d;", $season));
    if($result){
      //get the details
      $name = $result['name'];
      drupal_set_title(t('View season %season', array('%season' => $name)));
      $starts = date("d M y",$result['start']);
      $ends = date("d M y", $result['end']);
      $form['start'] = array('#value' => t('Starts: ').$starts, '#prefix' => '<div id="leaguesite_season_start">', '#suffix' => '</div>');
      $form['end'] = array('#value' => t(' Ends: ').$ends, '#prefix' => '<div id="leaguesite_season_end">', '#suffix' => '</div>');
      $form['leagues'] = array(
        '#type' => 'fieldset',
        '#title' => t('Leagues currently running in this season'),
        '#description' => t('Note, if you remove any of these after entering match information, you will remove all the team standings information from the tables for this season.'),
      );
      //get the leagues list and show them below
      $all_leagues = db_query("SELECT * FROM {leaguesite_league} ORDER BY {leaguesite_league}.lid;");
      $enabled_leagues_result = db_query("SELECT {leaguesite_relation}.lid FROM {leaguesite_relation}, {leaguesite_league} WHERE {leaguesite_relation}.sid=%d AND {leaguesite_relation}.relation_type = 'league' ORDER BY {leaguesite_league}.lid;", $season);
      
      while($row = db_fetch_array($enabled_leagues_result)){
        $enabled_leagues[$row['lid']] = 'TRUE';
      }
      
      while($row = db_fetch_array($all_leagues)){
        if(isset($enabled_leagues[$row['lid']])){
          //the current row is present in the league.
          $ticked = TRUE;
          $link = ' ('.l(t('View Teams'), 'admin/content/leaguesite/leagues/'.$season."/".$row['lid'].'/view').')';
          $link .= ' or ('.l(t('Edit Standings'), 'admin/content/leaguesite/standings/'.$season."/".$row['lid'].'/edit').')';
          //get the next enabled one
        }else{
          $ticked = FALSE;
          $link = '';
        }
        $form['leagues'][$row['lid']] = array(
          '#type' => 'checkbox',
          '#title' => $row['name'].$link,
          '#default_value' => $ticked,
        );
        $form['submit'] = array(
          '#type' => 'submit', '#value' => t('Make Changes')
        );
      }
    }else{
      drupal_not_found();
    }
  }else{
    drupal_not_found();
  }
  return $form;
}

/**
 * Function to deal with the submission of the season view - users can select what leagues are present in the season.
 * @param $form - the form being submitted
 * @param $form_state - values entered in the form
 */
function leaguesite_season_view_submit($form, $form_state){
  $season = arg(3);
  if(!is_numeric($season)){
    drupal_not_found();
  }else{
    //loop through the form league values and add/remove the league_relation as appropriate
    foreach($form_state['values'] as $id => $checked){
      //get the value from league_relation and check to see it matches the chosen state in the tickbox
      $result = db_fetch_array(db_query("SELECT * FROM {leaguesite_relation} WHERE lid = %d AND sid = %d AND relation_type = 'league';", check_plain($id), $season));
      if(($result == FALSE && $checked == 1) || ($result != FALSE && $checked == 0)){
        if($checked == 1){
          //if checked equals 1, then add the relation
          db_query("INSERT INTO {leaguesite_relation} (sid, lid, sport_id, relation_type) VALUES (%d, %d, -1, 'league')", $season, $id);
        }else if($checked == 0){
          //if checked equals 0, then delete the relation
          //call the hook to notify others of the deletion.
          module_invoke_all('leaguesite_relation_delete', intval($id));
          db_query("DELETE FROM {leaguesite_relation} WHERE lid = %d", check_plain($id));
        }
      }
    }
    drupal_set_message("Changes Saved");
  }
}
