<?php

//this file will contain all the code for the viewing of the leagues

function leaguesite_view_seasons(){
  $output = '';
  //determine if there is only one season. If so, then display a table with the leagues from the seasons.
  //If there are multiple seasons available, then display a selection.
  //ALSO, consider that there may be no seasons currently running!
  $result = db_query("SELECT r.sid AS season_id, r.relation_id AS relation_id, r.lid as league_id, s.name AS season_name, l.name AS league_name, l.weeks AS league_weeks FROM {leaguesite_relation} r LEFT JOIN {leaguesite_season} s ON r.sid = s.sid LEFT JOIN {leaguesite_league} l ON r.lid = l.lid WHERE r.relation_type = 'league' ORDER BY s.weight DESC, l.weight DESC;");
  $seasons = array();
  while($row = db_fetch_array($result)){

    $seasons[$row['relation_id']] = array('season_name' => $row['season_name'], 'season_id' => $row['season_id'], 'league_name' => $row['league_name'], 'league_weeks' => $row['league_weeks']);
  }
  if(count($seasons) == 0){
    //no result
    //check for past seasons, or no result
  }else if(count($seasons) == 1){
    //one result, get the leagues involved
    foreach($seasons AS $relation_id => $data){
      $leaguerows[] = array(
      l($data['league_name'], 'leaguesite/'.$relation_id),
      check_plain($data['league_weeks']),
      );

    }
    $leagueheader = array(t('League Name'), t('Weeks'));
    $output = theme('table', $leagueheader, $leaguerows);
  }else{
    $leaguerows = array();
    $leaguedata = array();
    //more than one result, maybe display the current season with the others in collapsible boxes? Maybe a pager for more than 5
    //use drupal_render to sort out fieldsets for seasons.
    foreach($seasons AS $relation_id => $data){
      $leaguedata[$data['season_id']] = array('#type' => 'fieldset', '#title' => $data['season_name']);
      $leaguerows[$data['season_id']][] = array(l($data['league_name'], 'leaguesite/'.$relation_id), check_plain($data['league_weeks']));
    }
    $leagueheader = array(t('League Name'), t('Weeks'));
    foreach($leaguerows AS $sid => $leagues){
      $rows = theme('table', $league_header, $leagues);
      $leaguedata[$sid]['list'] = array('#type' => 'markup', '#weight' => -1, '#value' => $rows);
    }
    $output = drupal_render($leaguedata);
  }

  return $output;
}

function leaguesite_view_standings(){
  $relation = arg(1);
  if(is_numeric($relation)){
    $info = db_fetch_array(db_query("SELECT {leaguesite_season}.name AS season, {leaguesite_league}.name AS league, {leaguesite_league}.lid FROM {leaguesite_relation}, {leaguesite_league}, {leaguesite_season} WHERE {leaguesite_relation}.relation_id = %d AND {leaguesite_league}.lid = {leaguesite_relation}.lid AND {leaguesite_season}.sid = {leaguesite_relation}.sid;", $relation));
    drupal_set_title(t('Viewing %league - %season', array('%league' => $info['league'], '%season' => $info['season'])));
    $league_id = $info['lid'];
    $breadcrumb = array(l(t('Home'), ''), l(t('Leagues'), 'leaguesite'));
    drupal_set_breadcrumb($breadcrumb);
    /*$result = db_query("SELECT {node}.title, {node}.nid, {leaguesite_standings}.*
     FROM {leaguesite_standings}, {node} WHERE {node}.nid = {leaguesite_standings}.team_id
     AND {leaguesite_standings}.relation_id = %d
     ORDER BY {leaguesite_standings}.won DESC, {leaguesite_standings}.drawn DESC;", $relation);*/
    $result = db_query("SELECT {leaguesite_standings}.*, {node}.title, {node}.nid, ({leaguesite_standings}.won_home + {leaguesite_standings}.won_home) + ({leaguesite_standings}.drawn_home + {leaguesite_standings}.drawn_away) + ({leaguesite_standings}.lost_home + {leaguesite_standings}.lost_away) AS played , (({leaguesite_standings}.won_home + {leaguesite_standings}.won_home) * {leaguesite_sports}.win) + (({leaguesite_standings}.drawn_home + {leaguesite_standings}.drawn_away)*{leaguesite_sports}.draw) + (({leaguesite_standings}.lost_home + {leaguesite_standings}.lost_away)*{leaguesite_sports}.loss) + ({leaguesite_standings}.bonus_home + {leaguesite_standings}.bonus_away) AS points
      FROM {leaguesite_standings}
      LEFT JOIN {leaguesite_relation} ON {leaguesite_standings}.relation_id = {leaguesite_relation}.relation_id
	  LEFT JOIN {node} ON {leaguesite_standings}.team_id = {node}.nid
	  LEFT JOIN {leaguesite_league} ON {leaguesite_relation}.lid = {leaguesite_league}.lid
	  LEFT JOIN {leaguesite_sports} ON {leaguesite_league}.sport_id = {leaguesite_sports}.sport_id
	  WHERE {leaguesite_relation}.relation_id = %d
	  AND {leaguesite_relation}.relation_type = 'league'
	  ORDER BY points DESC, ({leaguesite_standings}.goals_for_home + {leaguesite_standings}.goals_for_away) DESC", $relation
    );
    //get the teams
    while($row = db_fetch_array($result)){
      $leagueteam = $row['name'];
      $teams[] = array(
        'name' => l($row['title'], 'node/'.$row['nid']),
        'played' => $row['played'],
        'won_home' => $row['won_home'],
        'drawn_home' => $row['drawn_home'],
        'lost_home' => $row['lost_home'],
        'goals_for_home' => $row['goals_for_home'],
        'goals_against_home' => $row['goals_against_home'],
        'bonus_home' => $row['bonus_home'],
        'won_away' => $row['won_away'],
        'drawn_away' => $row['drawn_away'],
        'lost_away' => $row['lost_away'],
        'goals_for_away' => $row['goals_for_away'],
        'goals_against_away' => $row['goals_against_away'],
        'bonus_away' => $row['bonus_away'],
        'points' => $row['points'],
      );
    }

    //order the teams here, using the scoring method and adding the bonus points on.
    //$teams = _leaguesite_order_teams($teams);

    //remove the unwanted fields from the standings table
    $teamsheader = _leaguesite_get_standings_fields($teams, $league_id);
  }
  $output = theme('table', $teamsheader, $teams);
  return $output;
}

/**
 * This private function can be used to get the heading for a league. When I start adding the option to display different fields
 * @param int $league_id = will be the league id. For now this doesn't matter, but I can start displaying different fields for different leagues if I want.
 * @return unknown_type
 */
function _leaguesite_get_standings_fields(&$teams, $league_id = 0){
  return array(t('Team Name'), t('Played'), t('Won home'), t('Drawn home'), t('Lost home'), t('Scored home'), t('Conceded home'), t('Bonus home'), t('Won away'), t('Drawn away'), t('Lost away'), t('Scored away'), t('Conceded away'), t('Bonus away'), t('Points'));
}

/**
 *
 * @param unknown_type $teams
 * @return unknown_type
 */
function _leaguesite_order_teams($teams){
	//have an outer and an inner loop for a simple insertion sort. Might optimise this sometime in the future if needed.
	//bearing in mind, the database query already tries to order them, so they should be fairly OK.
	for($outcount = 0; $outcount < count($teams); ++$outcount){
		for($incount = 0; $incount < (count($teams)-1); ++$incount){
			if($teams[$incount]['points'] < $teams[$incount +1]['points']){
				//swap
				$teamtemp = $teams[$incount];
				$teams[$incount] = $teams[$incount+1];
				$teams[$incount+1] = $teamtemp;
			}
		}
	}
	return $teams;
}