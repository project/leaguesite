<?php
/**
 * Function to get and list the scoring methods of the leagues
 * @return $form listing the details of the scoring types available
 */
function leaguesite_scoring_list(){
	$leagues = db_query("SELECT * FROM {leaguesite_sports};");
	$count = 0;

	while($row = db_fetch_array($leagues)){
		$form[$row['sport_id']]['name'] = array('#value' => $row['name'],);
		$form[$row['sport_id']]['win'] = array('#value' => $row['win']);
		$form[$row['sport_id']]['draw'] = array('#value' => $row['draw']);
		$form[$row['sport_id']]['lose'] = array('#value' => $row['loss']);
		$form[$row['sport_id']]['configure'] = array('#value' => l(t('configure'), 'admin/content/leaguesite/sport/'. $row['sport_id'].'/edit'));
		$form[$row['sport_id']]['delete'] = array('#value' => $default ? '' : l(t('delete'), 'admin/content/leaguesite/sport/'.$row['sport_id'].'/delete'));
		++$count;
	}
	if($count == 0){
		$form['notice'] = array('#value' => 'No scoring methods currently exist.');
	}


	return $form;
}

/**
 * Theme the scoring output
 * @param Array $form - the form to render
 * @return $output - rendered output.
 */
function theme_leaguesite_scoring_list($form) {
	$sportrows = array();
	foreach ($form as $name => $element) {
		if(is_array($element['name'])){
			$sportrows[] = array(
			drupal_render($element['name']),
			drupal_render($element['win']),
			drupal_render($element['draw']),
			drupal_render($element['lose']),
			drupal_render($element['configure']),
			drupal_render($element['delete'])
			);
			unset($form[$name]);
		}
	}
	$seasonheader = array(t('Scoring System'), t('Win'), t('Draw'), t('Lose'), array('data' => t('Operations'), 'colspan' => 2));
	if(count($sportrows) > 0){
		$output = theme('table', $seasonheader, $sportrows);
	}
	$output .= l('Add a new sport', 'admin/content/leaguesite/sport/add');
	return $output;
}

function leaguesite_sport_add(){
	$breadcrumb = drupal_get_breadcrumb();
	$breadcrumb[] = l(t('Sports'), 'admin/content/leaguesite/sport');
	drupal_set_breadcrumb($breadcrumb);

	$form = array();
	//add a new season
	$form['sport_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Scoring type name'),
    '#description' => t('The name of the scoring type'),
    '#size' => 30,
    '#maxlength' => 64,
    '#required' => TRUE,
	);
	$form['sport_win'] = array(
    '#type' => 'textfield',
    '#title' => t('Points for a win'),  
    '#description' => t('Number of points a team achieves for a win.'),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => FALSE,
	);
	$form['sport_draw'] = array(
    '#type' => 'textfield',
    '#title' => t('Points for a draw'),
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t('Number of a points a team acheve for a draw.'),
	);
	$form['sport_lose'] = array(
    '#type' => 'textfield',
    '#title' => t('Points for a loss'),
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t('Number of points achieved for a loss.'),
	);
	  $form['subsitutions'] = array(
	'#type' => 'checkbox',
	'#title' => t('Substitutions'),
	'#description' => t('Check this box if the sport has subsitutions')
  );
  $form['penalties'] = array(
	'#type' => 'checkbox',
	'#title' => t('Penalties'),
	'#description' => t('Check this box if the sport has penalties (e.g. yellow and red cards)')
  );
  $form['submit'] = array(
    '#type' => 'submit', '#value' => t('Create Scoring Type'),
  );
  return $form;
}

/**
 * Function to add the sport to the database
 * @param unknown_type $form
 * @param unknown_type $form_state
 * @return none
 */
function leaguesite_sport_add_submit($form, $form_state){
  $sport_name = check_plain($form_state['values']['sport_name']);
  $sport_win = intval($form_state['values']['sport_win']);
  $sport_draw = intval($form_state['values']['sport_draw']);
  $sport_lose = intval($form_state['values']['sport_loss']);
  $penalties = intval($form_state['values']['penalties']);
  $substitutions = intval($form_state['values']['substitutions']);
  db_query("INSERT INTO {leaguesite_sports} (name, win, draw, loss, substitutions, penalties) VALUES ('%s', %d, %d, %d, %d, %d);", $sport_name, $sport_win, $sport_draw, $sport_lose, $substitutions, $penalties);
  //if we are creating a new sport, we also need to add a friendly option into the leaguesite_relation - one for each season
  $seasons = db_query('SELECT sid FROM {leaguesite_season};');
  $sport_id = db_last_insert_id('leaguesite_sports', 'sport_id');
  while($season = db_fetch_array($seasons)){
    db_query("INSERT INTO {leaguesite_relation} (sid, lid, sport_id, relation_type) VALUES (%d, %d, %d, 'friendly');", $season['sid'], -1, $sport_id);
  }

  drupal_set_message(t('Added sport %sport.', array('%sport' => $sport_name)));
}

function leaguesite_sport_edit(){
	$breadcrumb = drupal_get_breadcrumb();
	$breadcrumb[] = l(t('Sports'), 'admin/content/leaguesite/sport');
	drupal_set_breadcrumb($breadcrumb);

	$sport_id = arg(4);
	$sport = db_fetch_array(db_query("SELECT * FROM {leaguesite_sports} WHERE sport_id = %d", $sport_id));

	$form = array();
	$form['sport_id'] = array(
    '#value' => $sport_id,
    '#type' => 'hidden',
	);
	$form['sport_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Scoring type name'),
    '#description' => t('The name of the scoring type'),
    '#size' => 30,
    '#maxlength' => 64,
    '#default_value' => $sport['name'],
    '#required' => TRUE,
	);
	$form['sport_win'] = array(
    '#type' => 'textfield',
    '#title' => t('Points for a win'),  
    '#description' => t('Number of points a team achieves for a win.'),
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $sport['win'],
    '#required' => FALSE,
	);
	$form['sport_draw'] = array(
    '#type' => 'textfield',
    '#title' => t('Points for a draw'),
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $sport['draw'],
    '#description' => t('Number of a points a team acheve for a draw.'),
	);
	$form['sport_lose'] = array(
    '#type' => 'textfield',
    '#title' => t('Points for a loss'),
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $sport['loss'],
    '#description' => t('Number of points achieved for a loss.'),
  );
  $form['substitutions'] = array(
	'#type' => 'checkbox',
	'#title' => t('Substitutions'),
	'#description' => t('Check this box if the sport has subsitutions'),
	'#default_value' => $sport['substitutions'],
  );
  $form['penalties'] = array(
	'#type' => 'checkbox',
	'#title' => t('Penalties'),
	'#description' => t('Check this box if the sport has penalties (e.g. yellow and red cards)'),
	'#default_value' => $sport['penalties'],
  );
  $form['submit'] = array(
    '#type' => 'submit', '#value' => t('Amend scoring type'),
  );
  return $form;
}

/**
 * Function to submit the edited sport to the database
 * @param unknown_type $form
 * @param unknown_type $form_state
 * @return none
 */
function leaguesite_sport_edit_submit($form, $form_state){
	$sport_name = check_plain($form_state['values']['sport_name']);
	$sport_win = intval($form_state['values']['sport_win']);
	$sport_draw = intval($form_state['values']['sport_draw']);
	$sport_lose = intval($form_state['values']['sport_loss']);
	$sport_id = intval($form_state['values']['sport_id']);
	$substitutions = intval($form_state['values']['substitutions']);
    $penalties = intval($form_state['values']['penalties']);
    db_query("UPDATE {leaguesite_sports} SET name = '%s', win = %d, draw = %d, loss = %d, substitutions = %d, penalties = %d WHERE sport_id = %d;", $sport_name, $sport_win, $sport_draw, $sport_lose, $substitutions, $penalties, $sport_id);
	drupal_set_message(t('Edited sport %sport.', array('%sport' => $sport_name)));
}

function leaguesite_sport_delete(){
	$sport_id = arg(4);
	$sport = db_fetch_array(db_query('SELECT * FROM {leaguesite_sports} WHERE sport_id = %d;', $sport_id));

	if ($sport) {
		$form['sport'] = array('#type' => 'hidden', '#value' => $sport['sport_id']);
		$form['name'] = array('#type' => 'hidden', '#value' => $sport['name']);
		return confirm_form($form, t('Are you sure you want to delete the sport %sport?', array('%sport' => $sport['name'])), 'admin/content/leaguesite/sport', t('Warning, if you have leagues using this scoring type, this will cause problems calculating league standings.'), t('Delete'), t('Cancel'));
	}
	else {
		drupal_not_found();
	}
}

/**
 * Function to delete selected sport type
 */
function leaguesite_sport_delete_submit($form, $form_state){
	//TODO: I should check that this sport is not being used, otherwise I should refuse to delete it until all leagues have changed their sport. Otherwise we have trouble on our hands 
	//If we are deleting a sport, we should also delete the option for friendlies in this sport in leaguesite_relation
	$sport_id = $form_state['values']['sport'];
	
	//check its numeric and a sensible number.
	if(is_numeric($sport_id) && $sport_id > -1){
		db_query("DELETE FROM {leaguesite_sports} WHERE sport_id = %d", $sport_id);
		db_query('DELETE FROM {leaguesite_relation} WHERE sport_id = %d', $sport_id);
		drupal_set_message(t('Scoring type deleted.'));
	}
	drupal_goto('admin/content/leaguesite/sport');
}